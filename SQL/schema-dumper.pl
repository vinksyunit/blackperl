# BlackPerl is a piece of software designed to help researchers to manage their publications.

# Copyright (C) 2014  Maxime CATALDI, Vincent DEMONCHY, Xavier LOCOGE, François de BELLEFON.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either at version 2 of this licence, or (at your option) any other version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/

#!/usr/bin/perl
use strict;
use warnings;
use lib "lib";
use DBIx::Class::Schema::Loader qw/make_schema_at/;

warn "INC: @INC\n";

my $classe_base     = 'DB::BPDatabase';
my $repertoire_base = 'lib';
my $dsn = 'dbi:mysql:blackperl:kim.demonchy.eu';
my $user = 'bpp_distant';
my $password = 'pl1ok2ij3';

make_schema_at(
	       $classe_base,
	       {
		constraint  => qr/bpp_jourkey|bpp_journal|bpp_keyword|bpp_manage|bpp_proofreader|bpp_proofreading|bpp_pubkey|bpp_publication|bpp_status_has_bpp_publication|bpp_status|bpp_user|bpp_usertype/,
		relationships  => 1,
		components     => [qw/ InflateColumn::DateTime Core/],
		debug          => 1,
		dump_directory => $repertoire_base,
		skip_load_external => 0,
	       },
	       [ $dsn, $user, $password],
	      );

__END__
