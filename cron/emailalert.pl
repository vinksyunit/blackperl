# BlackPerl is a piece of software designed to help researchers to manage their publications.

# Copyright (C) 2014  Maxime CATALDI, Vincent DEMONCHY, Xavier LOCOGE, François de BELLEFON.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either at version 2 of this licence, or (at your option) any other version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/

use strict;
use warnings;
use utf8;
use DateTime;
use DateTime::Format::HTTP;
use Time::localtime;
use Config::Simple;
use FindBin qw( $RealBin );
use lib $RealBin.'/../lib';
use MIME::Lite::TT::HTML;
my $cfg = new Config::Simple('../config.ini');

my $dsn = $cfg->param("mysql.dsn");
use DB::BPDatabase;
my $schema = DB::BPDatabase->connect({dsn => $dsn,
	user => $cfg->param("mysql.user"),
	password => $cfg->param("mysql.password"),
	mysql_enable_utf8  => 1,
	on_connect_do     => [
             'SET NAMES utf8',
     ],
}); 

my $dt_now = DateTime->now;
my $dt_7days = $dt_now + DateTime::Duration->new(weeks => 1);
my $dt_14days = $dt_7days + DateTime::Duration->new(weeks => 1);

# print($dt_now->ymd.' '.$dt_7days->ymd.' '.$dt_14days->ymd);

my $res = $schema->resultset('BppStatus')->search({'me.status_actuel' => 'Y',
												   'me.estimated_date' => [$dt_now->ymd,$dt_7days->ymd,$dt_14days->ymd]
												   });
my @statuss = $res->all();

# Récupération email administrateurs
my $res_admin = $schema->resultset('BppUser')->search({'me.is_deleted' => 'N',
												   'bpp_usertype_idbpp_usertype' => '0'
												   });
my @admins = $res_admin->all();

# On configure le chemin d'accès aux Templates
my %options;
$options{INCLUDE_PATH} = $cfg->param("site.pathtemplatemail");
my $email_from = $cfg->param("email.from");

my $compteur_test = 0;
# Préparation des données pour le mail à l'auteur référent
for my $status (@statuss)
{
	my %params;

	my $auteur_ref = get_auteur_princ($status->bpp_publication_idbpp_publication->idbpp_publication);
	$params{url_site} = $cfg->param("site.uri_with_dispatch");
	$params{auteur_ref} = $auteur_ref;
	$params{status} =  $status;
	$params{publication} = $status->bpp_publication_idbpp_publication;
	# $auteur_ref->email
	my $msg = MIME::Lite::TT::HTML->new(
	            From        =>  $email_from,
	            To          =>  $auteur_ref->email,
	            Subject     =>  '[Gestion Publication] Tâche à finir bientôt : '.$status->type.' pour '.$status->bpp_publication_idbpp_publication->title,
	            Template    =>  {
	                                text    =>  'useremail.txt.tt',
	                                html    =>  'useremail.html.tt',
	                            },
	            TmplOptions =>  \%options,
	            TmplParams  =>  \%params,
	);

	# Require sendmail
	$msg->send();
	$compteur_test = 1;
}

# Préparation des données pour le mail aux administrateurs
if ($compteur_test == 1)
{
	for my $admin (@admins)
	{
		my %params;
		$params{url_site} = $cfg->param("site.uri_with_dispatch");
		$params{statuss} =  \@statuss;
		# $auteur_ref->email
		my $msg = MIME::Lite::TT::HTML->new(
		            From        =>  $email_from,
		            To          =>  $admin->email,
		            Subject     =>  '[Gestion Publication] Alerte tâches '.$dt_now->ymd,
		            Template    =>  {
		                                text    =>  'adminemail.txt.tt',
		                                html    =>  'adminemail.html.tt',
		                            },
		            TmplOptions =>  \%options,
		            TmplParams  =>  \%params,
		);

		# Require sendmail
		$msg->send();
	}
}


sub get_auteur_princ {
  my $id_pub = $_[0];
  my $res = $schema->resultset('BppManage')->search({'me.idbpp_publication' => $id_pub,
                                                    'me.rank' => "1"});
  my $manage = $res->first();
  my $auteur_princ = $manage->idbpp_user;
  return $auteur_princ;
}