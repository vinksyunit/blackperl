use utf8;
package DB::BPDatabase;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07025 @ 2014-02-25 09:55:36
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:qeckLMeZtnQ2UUHI8wdsGQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
