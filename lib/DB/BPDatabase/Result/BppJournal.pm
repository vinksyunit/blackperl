use utf8;
package DB::BPDatabase::Result::BppJournal;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

DB::BPDatabase::Result::BppJournal

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::Core>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");

=head1 TABLE: C<bpp_journal>

=cut

__PACKAGE__->table("bpp_journal");

=head1 ACCESSORS

=head2 idbpp_journal

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 title

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 subject

  data_type: 'text'
  is_nullable: 1

=head2 template_link

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 impact_factor

  data_type: 'decimal'
  is_nullable: 1
  size: [4,0]

=head2 is_deleted

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=cut

__PACKAGE__->add_columns(
  "idbpp_journal",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "title",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "subject",
  { data_type => "text", is_nullable => 1 },
  "template_link",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "impact_factor",
  { data_type => "decimal", is_nullable => 1, size => [4, 0] },
  "is_deleted",
  { data_type => "varchar", is_nullable => 1, size => 45 },
);

=head1 PRIMARY KEY

=over 4

=item * L</idbpp_journal>

=back

=cut

__PACKAGE__->set_primary_key("idbpp_journal");

=head1 RELATIONS

=head2 bpp_jourkeys

Type: has_many

Related object: L<DB::BPDatabase::Result::BppJourkey>

=cut

__PACKAGE__->has_many(
  "bpp_jourkeys",
  "DB::BPDatabase::Result::BppJourkey",
  { "foreign.idbpp_journal" => "self.idbpp_journal" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 bpp_publications

Type: has_many

Related object: L<DB::BPDatabase::Result::BppPublication>

=cut

__PACKAGE__->has_many(
  "bpp_publications",
  "DB::BPDatabase::Result::BppPublication",
  { "foreign.idbpp_journal" => "self.idbpp_journal" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 idbpp_keywords

Type: many_to_many

Composing rels: L</bpp_jourkeys> -> idbpp_keyword

=cut

__PACKAGE__->many_to_many("idbpp_keywords", "bpp_jourkeys", "idbpp_keyword");


# Created by DBIx::Class::Schema::Loader v0.07025 @ 2014-02-19 18:40:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:B8P1TzbXuo0OsPTPcVYxyA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
