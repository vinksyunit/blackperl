use utf8;
package DB::BPDatabase::Result::BppPublication;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

DB::BPDatabase::Result::BppPublication

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::Core>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");

=head1 TABLE: C<bpp_publication>

=cut

__PACKAGE__->table("bpp_publication");

=head1 ACCESSORS

=head2 idbpp_publication

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 idbpp_journal

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 title

  data_type: 'varchar'
  is_nullable: 1
  size: 1024

=head2 subject

  data_type: 'text'
  is_nullable: 1

=head2 abstract

  data_type: 'text'
  is_nullable: 1

=head2 main_ideas

  data_type: 'varchar'
  is_nullable: 1
  size: 1024

=head2 is_deleted

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=cut

__PACKAGE__->add_columns(
  "idbpp_publication",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "idbpp_journal",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "title",
  { data_type => "varchar", is_nullable => 1, size => 1024 },
  "subject",
  { data_type => "text", is_nullable => 1 },
  "abstract",
  { data_type => "text", is_nullable => 1 },
  "main_ideas",
  { data_type => "varchar", is_nullable => 1, size => 1024 },
  "is_deleted",
  { data_type => "varchar", is_nullable => 1, size => 45 },
);

=head1 PRIMARY KEY

=over 4

=item * L</idbpp_publication>

=back

=cut

__PACKAGE__->set_primary_key("idbpp_publication");

=head1 RELATIONS

=head2 bpp_pubkeys

Type: has_many

Related object: L<DB::BPDatabase::Result::BppPubkey>

=cut

__PACKAGE__->has_many(
  "bpp_pubkeys",
  "DB::BPDatabase::Result::BppPubkey",
  { "foreign.idbpp_publication" => "self.idbpp_publication" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 bpp_statuses

Type: has_many

Related object: L<DB::BPDatabase::Result::BppStatus>

=cut

__PACKAGE__->has_many(
  "bpp_statuses",
  "DB::BPDatabase::Result::BppStatus",
  {
    "foreign.bpp_publication_idbpp_publication" => "self.idbpp_publication",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 bpps_manage

Type: has_many

Related object: L<DB::BPDatabase::Result::BppManage>

=cut

__PACKAGE__->has_many(
  "bpps_manage",
  "DB::BPDatabase::Result::BppManage",
  { "foreign.idbpp_publication" => "self.idbpp_publication" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 bpps_proofreading

Type: has_many

Related object: L<DB::BPDatabase::Result::BppProofreading>

=cut

__PACKAGE__->has_many(
  "bpps_proofreading",
  "DB::BPDatabase::Result::BppProofreading",
  { "foreign.idbpp_publication" => "self.idbpp_publication" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 idbpp_journal

Type: belongs_to

Related object: L<DB::BPDatabase::Result::BppJournal>

=cut

__PACKAGE__->belongs_to(
  "idbpp_journal",
  "DB::BPDatabase::Result::BppJournal",
  { idbpp_journal => "idbpp_journal" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 idbpp_keywords

Type: many_to_many

Composing rels: L</bpp_pubkeys> -> idbpp_keyword

=cut

__PACKAGE__->many_to_many("idbpp_keywords", "bpp_pubkeys", "idbpp_keyword");


# Created by DBIx::Class::Schema::Loader v0.07025 @ 2014-02-19 18:40:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:auqQgNP8SQ2guOTpngWyCw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
