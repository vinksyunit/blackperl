use utf8;
package DB::BPDatabase::Result::BppUser;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

DB::BPDatabase::Result::BppUser

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::Core>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");

=head1 TABLE: C<bpp_user>

=cut

__PACKAGE__->table("bpp_user");

=head1 ACCESSORS

=head2 idbpp_user

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 surname

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 login

  data_type: 'varchar'
  is_nullable: 0
  size: 45

				

=head2 password

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 epi

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 email

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 position

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 bpp_usertype_idbpp_usertype

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 is_deleted

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=cut

__PACKAGE__->add_columns(
  "idbpp_user",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "surname",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "login",
  { data_type => "varchar", is_nullable => 0, size => 45 },
  "password",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "epi",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "email",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "position",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "bpp_usertype_idbpp_usertype",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "is_deleted",
  { data_type => "varchar", is_nullable => 1, size => 45 },
);

=head1 PRIMARY KEY

=over 4

=item * L</idbpp_user>

=back

=cut

__PACKAGE__->set_primary_key("idbpp_user");

=head1 RELATIONS

=head2 bpp_usertype_idbpp_usertype

Type: belongs_to

Related object: L<DB::BPDatabase::Result::BppUsertype>

=cut

__PACKAGE__->belongs_to(
  "bpp_usertype_idbpp_usertype",
  "DB::BPDatabase::Result::BppUsertype",
  { idbpp_usertype => "bpp_usertype_idbpp_usertype" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 bpps_manage

Type: has_many

Related object: L<DB::BPDatabase::Result::BppManage>

=cut

__PACKAGE__->has_many(
  "bpps_manage",
  "DB::BPDatabase::Result::BppManage",
  { "foreign.idbpp_user" => "self.idbpp_user" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07025 @ 2014-02-19 18:40:51
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:P/pElevT3h3yLqfYOWKeug


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
