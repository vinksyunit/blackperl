use utf8;
package DB::BPDatabase::Result::BppProofreading;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

DB::BPDatabase::Result::BppProofreading

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::Core>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");

=head1 TABLE: C<bpp_proofreading>

=cut

__PACKAGE__->table("bpp_proofreading");

=head1 ACCESSORS

=head2 idbpp_proofreader

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 idbpp_publication

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 proofreading_status

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=cut

__PACKAGE__->add_columns(
  "idbpp_proofreader",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "idbpp_publication",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "proofreading_status",
  { data_type => "varchar", is_nullable => 1, size => 255 },
);

=head1 RELATIONS

=head2 idbpp_proofreader

Type: belongs_to

Related object: L<DB::BPDatabase::Result::BppProofreader>

=cut

__PACKAGE__->belongs_to(
  "idbpp_proofreader",
  "DB::BPDatabase::Result::BppProofreader",
  { idbpp_proofreader => "idbpp_proofreader" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 idbpp_publication

Type: belongs_to

Related object: L<DB::BPDatabase::Result::BppPublication>

=cut

__PACKAGE__->belongs_to(
  "idbpp_publication",
  "DB::BPDatabase::Result::BppPublication",
  { idbpp_publication => "idbpp_publication" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);


# Created by DBIx::Class::Schema::Loader v0.07025 @ 2014-02-19 18:40:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:MUWzVXFa0djViZYQ11pJJA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
