use utf8;
package DB::BPDatabase::Result::BppStatus;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

DB::BPDatabase::Result::BppStatus

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::Core>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");

=head1 TABLE: C<bpp_status>

=cut

__PACKAGE__->table("bpp_status");

=head1 ACCESSORS

=head2 idbpp_status

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 bpp_publication_idbpp_publication

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 type

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 beginning_date

  data_type: 'date'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 estimated_date

  data_type: 'date'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 final_date

  data_type: 'date'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 status_actuel

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 status_detail

  data_type: 'text'
  is_nullable: 1

=head2 status_is_deleted

  data_type: 'varchar'
  is_nullable: 0
  size: 45

=cut

__PACKAGE__->add_columns(
  "idbpp_status",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "bpp_publication_idbpp_publication",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "type",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "beginning_date",
  { data_type => "date", datetime_undef_if_invalid => 1, is_nullable => 1 },
  "estimated_date",
  { data_type => "date", datetime_undef_if_invalid => 1, is_nullable => 1 },
  "final_date",
  { data_type => "date", datetime_undef_if_invalid => 1, is_nullable => 1 },
  "status_actuel",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "status_detail",
  { data_type => "text", is_nullable => 1 },
  "status_is_deleted",
  { data_type => "varchar", is_nullable => 0, size => 45 },
);

=head1 PRIMARY KEY

=over 4

=item * L</idbpp_status>

=back

=cut

__PACKAGE__->set_primary_key("idbpp_status");

=head1 RELATIONS

=head2 bpp_publication_idbpp_publication

Type: belongs_to

Related object: L<DB::BPDatabase::Result::BppPublication>

=cut

__PACKAGE__->belongs_to(
  "bpp_publication_idbpp_publication",
  "DB::BPDatabase::Result::BppPublication",
  { idbpp_publication => "bpp_publication_idbpp_publication" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);


# Created by DBIx::Class::Schema::Loader v0.07025 @ 2014-02-19 18:40:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:QwTuxEl/ihE92jpLGL5uYw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
