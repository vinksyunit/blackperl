use utf8;
package DB::BPDatabase::Result::BppJourkey;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

DB::BPDatabase::Result::BppJourkey

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::Core>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");

=head1 TABLE: C<bpp_jourkey>

=cut

__PACKAGE__->table("bpp_jourkey");

=head1 ACCESSORS

=head2 idbpp_keyword

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 idbpp_journal

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "idbpp_keyword",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "idbpp_journal",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</idbpp_keyword>

=item * L</idbpp_journal>

=back

=cut

__PACKAGE__->set_primary_key("idbpp_keyword", "idbpp_journal");

=head1 RELATIONS

=head2 idbpp_journal

Type: belongs_to

Related object: L<DB::BPDatabase::Result::BppJournal>

=cut

__PACKAGE__->belongs_to(
  "idbpp_journal",
  "DB::BPDatabase::Result::BppJournal",
  { idbpp_journal => "idbpp_journal" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 idbpp_keyword

Type: belongs_to

Related object: L<DB::BPDatabase::Result::BppKeyword>

=cut

__PACKAGE__->belongs_to(
  "idbpp_keyword",
  "DB::BPDatabase::Result::BppKeyword",
  { idbpp_keyword => "idbpp_keyword" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);


# Created by DBIx::Class::Schema::Loader v0.07025 @ 2014-02-19 18:40:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:JeF63/t2HfRK0Wy0rE3Hng


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
