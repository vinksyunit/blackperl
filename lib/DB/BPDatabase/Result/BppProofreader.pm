use utf8;
package DB::BPDatabase::Result::BppProofreader;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

DB::BPDatabase::Result::BppProofreader

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::Core>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");

=head1 TABLE: C<bpp_proofreader>

=cut

__PACKAGE__->table("bpp_proofreader");

=head1 ACCESSORS

=head2 idbpp_proofreader

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 pf_name

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 pf_cost

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 pf_time

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 pf_comment

  data_type: 'text'
  is_nullable: 1

=head2 is_deleted

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=cut

__PACKAGE__->add_columns(
  "idbpp_proofreader",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "pf_name",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "pf_cost",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "pf_time",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "pf_comment",
  { data_type => "text", is_nullable => 1 },
  "is_deleted",
  { data_type => "varchar", is_nullable => 1, size => 45 },
);

=head1 PRIMARY KEY

=over 4

=item * L</idbpp_proofreader>

=back

=cut

__PACKAGE__->set_primary_key("idbpp_proofreader");

=head1 RELATIONS

=head2 bpps_proofreading

Type: has_many

Related object: L<DB::BPDatabase::Result::BppProofreading>

=cut

__PACKAGE__->has_many(
  "bpps_proofreading",
  "DB::BPDatabase::Result::BppProofreading",
  { "foreign.idbpp_proofreader" => "self.idbpp_proofreader" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07025 @ 2014-02-19 18:40:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:rVJzAlVawX0O+gmVrAURpQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
