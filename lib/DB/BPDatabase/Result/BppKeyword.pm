use utf8;
package DB::BPDatabase::Result::BppKeyword;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

DB::BPDatabase::Result::BppKeyword

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::Core>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");

=head1 TABLE: C<bpp_keyword>

=cut

__PACKAGE__->table("bpp_keyword");

=head1 ACCESSORS

=head2 idbpp_keyword

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 word

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 is_deleted

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=cut

__PACKAGE__->add_columns(
  "idbpp_keyword",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "word",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "is_deleted",
  { data_type => "varchar", is_nullable => 1, size => 45 },
);

=head1 PRIMARY KEY

=over 4

=item * L</idbpp_keyword>

=back

=cut

__PACKAGE__->set_primary_key("idbpp_keyword");

=head1 RELATIONS

=head2 bpp_jourkeys

Type: has_many

Related object: L<DB::BPDatabase::Result::BppJourkey>

=cut

__PACKAGE__->has_many(
  "bpp_jourkeys",
  "DB::BPDatabase::Result::BppJourkey",
  { "foreign.idbpp_keyword" => "self.idbpp_keyword" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 bpp_pubkeys

Type: has_many

Related object: L<DB::BPDatabase::Result::BppPubkey>

=cut

__PACKAGE__->has_many(
  "bpp_pubkeys",
  "DB::BPDatabase::Result::BppPubkey",
  { "foreign.idbpp_keyword" => "self.idbpp_keyword" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 idbpp_journals

Type: many_to_many

Composing rels: L</bpp_jourkeys> -> idbpp_journal

=cut

__PACKAGE__->many_to_many("idbpp_journals", "bpp_jourkeys", "idbpp_journal");

=head2 idbpp_publications

Type: many_to_many

Composing rels: L</bpp_pubkeys> -> idbpp_publication

=cut

__PACKAGE__->many_to_many("idbpp_publications", "bpp_pubkeys", "idbpp_publication");


# Created by DBIx::Class::Schema::Loader v0.07025 @ 2014-02-19 18:40:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:TkUbT5zAV8PrkrnYyqB4zw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
