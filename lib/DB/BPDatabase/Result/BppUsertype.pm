use utf8;
package DB::BPDatabase::Result::BppUsertype;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

DB::BPDatabase::Result::BppUsertype

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::Core>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "Core");

=head1 TABLE: C<bpp_usertype>

=cut

__PACKAGE__->table("bpp_usertype");

=head1 ACCESSORS

=head2 idbpp_usertype

  data_type: 'integer'
  is_nullable: 0

=head2 nom

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=cut

__PACKAGE__->add_columns(
  "idbpp_usertype",
  { data_type => "integer", is_nullable => 0 },
  "nom",
  { data_type => "varchar", is_nullable => 1, size => 45 },
);

=head1 PRIMARY KEY

=over 4

=item * L</idbpp_usertype>

=back

=cut

__PACKAGE__->set_primary_key("idbpp_usertype");

=head1 RELATIONS

=head2 bpp_users

Type: has_many

Related object: L<DB::BPDatabase::Result::BppUser>

=cut

__PACKAGE__->has_many(
  "bpp_users",
  "DB::BPDatabase::Result::BppUser",
  { "foreign.bpp_usertype_idbpp_usertype" => "self.idbpp_usertype" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07025 @ 2014-02-19 18:40:51
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:veIbxcMcAqNEMWdc9fSMGw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
