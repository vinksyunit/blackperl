# BlackPerl is a piece of software designed to help researchers to manage their publications.

# Copyright (C) 2014  Maxime CATALDI, Vincent DEMONCHY, Xavier LOCOGE, François de BELLEFON.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the MIT License or the GNU General Public License as published by
# the Free Software Foundation, either at version 2 of this licence, or (at your option) any other version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/

package BlackPerl;
use strict;
use warnings;
# on indique à Perl que le code est en utf-8
use utf8;
# le framework de contrôle (le C de MVC)
use Dancer ':syntax';
# pour gérer les URI
use URI;
use URI::QueryParam;
# facilite l'accès aux classes DBI (mot-clé: schema) :
use Dancer::Plugin::DBIC;
# use Dancer::Plugin::Email;
use DBI;
# pour gérer les dates au format HTTP
use DateTime;
use DateTime::Format::HTTP;
use Time::localtime;
use Digest::SHA1  qw(sha1 sha1_hex sha1_base64);
use Config::Simple;
my $cfg = new Config::Simple('../config.ini');

# affichage de debug des structures Perl :
use Data::Dumper;
use DBIx::Class::QueryLog;
use DBIx::Class::QueryLog::Analyzer;
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);
use File::Slurp qw( slurp );
$Data::Dumper::Indent = 1;

# Retraitement de l'enregistrement ckeditor pour l'extraction. 
#Conversion HTML -> Plain text
use HTML::FormatText;
use HTML::TreeBuilder 5 -weak;

our $VERSION = '0.1';

## Dossier dans lequel on veut exporter les données Excel.
my $path = $cfg->param("site.pathexcel");

## -----------------------------------------------------------------------------
## appelé juste avant la génération d'un template : transmission de
## données complémentaires au template
hook before_template => sub {
  my $tokens = shift;

  $tokens->{uri_base} = URI->new(request->uri_base);
  $tokens->{myurl} = URI->new(request->uri);

  $tokens->{url_get_css} = uri_for("/css"); # url pour obtenir la css
  $tokens->{url_get_images} = uri_for("/images"); # url pour obtenir les images
  $tokens->{url_get_javascripts} = uri_for("/javascripts"); # url pour obtenir le javascript
  $tokens->{url_get_plugins} = uri_for("/plugins"); # url pour obtenir les plugins

  # Récupération du contenu sur l'utilisateur connecté
  if (check_user()) {
    $tokens->{current_user} = recup_user_from_session();

    if (check_user_is_admin()){
      $tokens->{is_admin} = true;
    }
    else {
      $tokens->{is_admin} = false;
    }
  }
};

## ------------------------------------------------------------
##  Les routes
## ------------------------------------------------------------

### route principale '/'
get '/' => sub {
  return redirect '/co' if !check_user();

  vue_publication_perso();
};

# ------------------------------------------------------------
#  Publication
# ------------------------------------------------------------

### route '/pub/show'
any ['get', 'post'] => '/pub/show' => sub {
  return redirect '/co' if !check_user();

  vue_publication_perso();

};

### route '/pub/show/admin'
get '/pub/show/admin' => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  
  vue_publication_admin();
};

### route '/pub/show/:id' en GET
get qr{/pub/show/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie

  vue_publication_details($id);
};

### route '/pub/add' en GET
get qr{/pub/add} => sub {
  return redirect '/co' if !check_user();

  lancement_ajout_publication();
};

### route '/pub/add' en POST
post qr{/pub/add} => sub {
  return redirect '/co' if !check_user();

  return lancement_ajout_publication() if (param("autor")==0);
  return lancement_ajout_publication() if (param("revue")==0);

  ajout_de_la_publication();
};

### route '/pub/edit/:id' en GET
get qr{/pub/edit/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();

  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie

  # On vérifie que la personne est un admin ou l'auteur référent
  my $test=check_user_is_referent($id);
  return redirect '/' if (!check_user_is_admin() && !$test);

  lancement_edit_publication($id);

};

### route '/pub/edit/:id' en POST
post qr{/pub/edit/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();

  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie

  return lancement_edit_publication($id) if (param("autor")==0);
  return lancement_edit_publication($id) if (param("revue")==0);

  # On vérifie que la personne est un admin ou l'auteur référent
  my $test=check_user_is_referent($id);
  return redirect '/' if (!check_user_is_admin() && !$test);
  
  edition_de_la_publication($id);
};

### route '/pub/duplicate/:id' en GET
get qr{/pub/duplicate/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();

  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie

  # On vérifie que la personne est un admin ou l'auteur référent
  my $test=check_user_is_referent($id);
  return redirect '/' if (!check_user_is_admin() && !$test);

  lancement_duplication_publication($id);
};

### route '/pub/duplicate/:id' en POST
post qr{/pub/duplicate/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();

  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie

  return lancement_duplication_publication($id) if (param("autor")==0);
  return lancement_duplication_publication($id) if (param("revue")==0);

  # On vérifie que la personne est un admin ou l'auteur référent
  my $test=check_user_is_referent($id);
  return redirect '/' if (!check_user_is_admin() && !$test);

  duplication_de_la_publication();
};

### route '/pub/remove/:id' en GET
get qr{/pub/remove/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();

  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie

  # On vérifie que la personne est un admin ou l'auteur référent
  my $test=check_user_is_referent($id);
  return redirect '/' if (!check_user_is_admin() && !$test);

  suppr_publication($id);
};

### route '/pub/remove/:id' en POST
post qr{/pub/remove/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();

  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie

  # On vérifie que la personne est un admin ou l'auteur référent
  my $test=check_user_is_referent($id);
  return redirect '/' if (!check_user_is_admin() && !$test);

  my $message = "La publication ".$id." a été ";
  my %hashParamVerif = (
      "is_deleted" => 'Y',
    );
  my $publication = schema->resultset('BppPublication')->find($id);
  if ($publication != undef) {
    $publication->update(\%hashParamVerif);
    $message = $message."supprimé.";
  }

  vue_publication_modif();
};

### route '/pub/recover/:id' en GET
get qr{/pub/recover/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie
  template 'index';

  recover_publication($id);
};

### route '/pub/recover/:id' en POST
post qr{/pub/recover/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie

  my $message = "La publication ".$id." a été ";
  my %hashParamVerif = (
      "is_deleted" => 'N',
    );
  my $publication = schema->resultset('BppPublication')->find($id);
  if ($publication != undef) {
    $publication->update(\%hashParamVerif);
    $message = $message."récupérée.";
  }

  var msgs => $message;

  vue_publication_modif();

};

# ------------------------------------------------------------
#  Gestion des fichiers de publications
# ------------------------------------------------------------

### route '/file/(id)' en GET
get '/file/(?<id>\d+)' => sub {
  return redirect '/co' if !check_user();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  my $test_droit_user=0;

  if (check_user_is_referent($id) || check_user_is_admin())
  {
    $test_droit_user = 1;
  }
  vue_file_manager($id, $test_droit_user);
};

post '/file/(?<id>\d+)/upload' => sub {
  return redirect '/co' if !check_user();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  my $test_droit_user=0;

  if (check_user_is_referent($id) || check_user_is_admin())
  {
    $test_droit_user = 1;
  }

  return redirect '/' if !$test_droit_user;
  
  my $uploaded_file = request->upload('file_to_add');
  my $msgs;

  my $new_filename = $uploaded_file->filename;
  $new_filename =~ s/\s+/_/g;

  if ($uploaded_file->copy_to($cfg->param("site.pathdatapub")."/".$id."/".$new_filename))
  {
    $msgs = "Le fichier ".$new_filename." a été ajouté.";
  }
  else
  {
    $msgs = "Le fichier ".$new_filename." n'a pas été pu être ajouté.";
  }

  var msgs => $msgs;

  vue_file_manager($id, $test_droit_user);
};

get '/file/(?<id>\d+)/delete/(?<name>\S+)' => sub {
  return redirect '/co' if !check_user();
  my $id = captures->{id};
  my $name = captures->{name};
  delete params->{captures};
  my $test_droit_user=0;

  if (check_user_is_referent($id) || check_user_is_admin())
  {
    $test_droit_user = 1;
  }

  return redirect '/' if !$test_droit_user;

  my $msgs = suppr_file($cfg->param("site.pathdatapub").'/'.$id.'/'.$name);
  var msgs => $msgs;

  vue_file_manager($id, $test_droit_user);
};

get '/file/(?<id>\d+)/download/(?<name>\S+)' => sub {
  return redirect '/co' if !check_user();
  my $id = captures->{id};
  my $name = captures->{name}; # le paramètre id est dans l'URL
  delete params->{captures};

  return send_file($cfg->param("site.pathdatapub").'/'.$id.'/'.$name, system_path => 1, filename =>$name);
};

get '/file/(?<id>\d+)/downloadpdf/(?<name>\S+)' => sub {
  return redirect '/co' if !check_user();
  my $id = captures->{id};
  my $name = captures->{name}; # le paramètre id est dans l'URL
  delete params->{captures};
  header 'Content-Type' => 'application/pdf';

  my $output = slurp($cfg->param("site.pathdatapub").'/'.$id.'/'.$name);

  return $output;
};



# ------------------------------------------------------------
#  Gestion de la connection
# ------------------------------------------------------------

### route '/co' en GET
get '/co' => sub {
  session->destroy();
  set layout => 'main';
  template 'index';
};

### route '/co' en POST
post '/co'=> sub {
  my $params = request->params;

  # Génération table des paramètres
  my %hashParamVerif = (
      "user_login" => lc(param("identification")),
      "user_password" => param("motdepasse"),
  );

  co_user(\%hashParamVerif);
};

### route '/co/pass' en GET
get '/co/pass' => sub {
  set layout => 'main';
  template 'newpassword';
};

### route '/co/pass' en POST
post '/co/pass' => sub {
  my $params = request->params;

  # Génération table des paramètres
  my %hashParamVerif = (
      "user_login" => lc(param("identification")),
      "user_email" => param("email"),
  );

  my $msgs = password_recup(\%hashParamVerif);
  set layout => 'main';
  template 'index', {
    msgs => $msgs,
  };
};


# ------------------------------------------------------------
#  Gestion des utilisateurs
# ------------------------------------------------------------

### route '/user/list' en GET
get '/user/list' => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  vue_liste_user();
};

### route '/user/add' en POST
post '/user/add' => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $params = request->params;

  # Génération table des paramètres
  my %hashParamVerif = (
      "user_login" => lc(param("login")),
      "user_name" => param("nom"),
      "user_surname" => param("prenom"),
      "user_password1" => param("password1"),
      "user_password2" => param("password2"),
      "user_email" => param("email"),
      "user_droit" => param("modif_droit"),
      "user_role" => param("modif_role"),
      "user_epi" => param("modif_epi"),
  );

  add_user(\%hashParamVerif);
};

### route '/user/add' en GET
get '/user/add' => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  vue_add_user();
};

### route '/user/edit/:id' en GET
get '/user/edit/(?<id>\d+)' => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  vue_edit_user($id);
};

### route '/user/edit/:id' en POST
post '/user/edit/(?<id>\d+)' => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
	my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  my $params = request->params;

  # Génération table des paramètres
  my %hashParamVerif = (
      "id" => $id,
      "user_login" => lc(param("login")),
      "user_name" => param("nom"),
      "user_surname" => param("prenom"),
      "user_password1" => param("password1"),
      "user_password2" => param("password2"),
      "user_email" => param("email"),
      "user_droit" => param("modif_droit"),
      "user_role" => param("modif_role"),
      "user_epi" => param("modif_epi"),
  );

  edit_user(\%hashParamVerif);
};

### route '/profil' en GET
get '/profil' => sub {
  return redirect '/co' if !check_user();
  my $id = recup_user_from_session()->idbpp_user;
  vue_edit_user($id);
};

### route '/profil' en POST
post '/profil' => sub {
  return redirect '/co' if !check_user();

  my $id = recup_user_from_session()->idbpp_user; # le paramètre id est dans l'URL

  my $params = request->params;

  # Génération table des paramètres
  my %hashParamVerif = (
      "id" => $id,
      "user_login" => lc(param("login")),
      "user_name" => param("nom"),
      "user_surname" => param("prenom"),
      "user_password1" => param("password1"),
      "user_password2" => param("password2"),
      "user_email" => param("email"),
      "user_role" => param("modif_role"),
      "user_epi" => param("modif_epi"),
  );

  edit_user_no_admin(\%hashParamVerif);

};

### route '/user/recover/:id' en POST
get '/user/recover/(?<id>\d+)' => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};

  recover_user($id);
};

### route '/user/suppr/:id' en GET
get '/user/suppr/(?<id>\d+)' => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  vue_delete_user($id);
};

### route '/user/suppr/:id' en POST
post '/user/suppr/(?<id>\d+)' => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};

  suppr_user($id);
};

# ------------------------------------------------------------
#  Gestion des mots cles
# ------------------------------------------------------------

### route '/keyword' en GET
any ['get', 'post'] => '/keyword' => sub{
  return redirect '/co' if !check_user();
  vue_liste_keyword();
};

### route '/keyword/add' en POST
post '/keyword/add' => sub {
  return redirect '/co' if !check_user();
  my $params = request->params;

  add_keyword(param("addmotcle"));
  vue_liste_keyword();
};

### route '/keyword/addJS' en POST
post '/keyword/addJS'=>sub{
  return '-1' if !check_user();
  my $params = request->params;

  return add_keyword(param("addmotcle"));
};

### route '/keyword/(id)' en GET
get '/keyword/(?<id>\d+)' => sub {
  return redirect '/co' if !check_user();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};

  vue_contenu_keyword($id);
};

# ------------------------------------------------------------
#  Gestion des correcteurs
# ------------------------------------------------------------

### route '/corr'
any ['get', 'post'] => '/corr' => sub {
  return redirect '/co' if !check_user();
  # return redirect '/' if !check_user_is_admin();
  vue_liste_correcteur();
};

### route '/corr/edit' en GET
get '/corr/edit/(?<id>\d+)'=>sub{
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};

  
  edit_correcteur($id);
};

get '/corr/recover/(?<id>\d+)'=>sub{
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};

  recover_corr($id);
};

### route '/corr/edit' en POST
post '/corr/edit/(?<id>\d+)'=>sub{
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};

  my $params = request->params;


  # Création de la table des paramètres
  my %hashParam = ( "cor_nom"     => "pf_name",
          "cor_cout" => "pf_cost",
          "cor_commentaire"   => "pf_comment",
          "cor_time"   => "pf_time",);

  # A ajouter, vérification des paramètres
  # Génération table des paramètres
  my %hashParamVerif = (
      "pf_name" => param("cor_nom"),
      "pf_cost" => param("cor_cout"),
      "pf_comment" => param("cor_commentaire"),
      "pf_time" => param("cor_time"),
      "pf_comment" => param("cor_commentaire"),
      "is_deleted" => 'N',
    );

  # Modification ou insertion du correcteur
  my $message = "Le correcteur ".$hashParam{"cor_nom"}." a été ";

  my $correcteur = schema->resultset('BppProofreader')->find($id);
  if ($correcteur == undef) {
    $correcteur = schema->resultset('BppProofreader')->create(\%hashParamVerif);
    $message = $message."ajouté.";
  }
  else {
    $correcteur->update(\%hashParamVerif);
    $message = $message."modifié.";
  }

  var msgs => $message;
  vue_liste_correcteur();
};

### route '/corr/add' en GET
get '/corr/add'=>sub{
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  template 'corredit', {
    type => "ajout",
  };
};

### route '/corr/add' en POST
post '/corr/add'=>sub{
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $params = request->params;


  # Création de la table des paramètres
  my %hashParam = ( "cor_nom"     => "pf_name",
          "cor_cout" => "pf_cost",
          "cor_commentaire"   => "pf_comment",
          "cor_time"   => "pf_time",);

  # A ajouter, vérification des paramètres
  # Génération table des paramètres
  my %hashParamVerif = (
      "pf_name" => param("cor_nom"),
      "pf_cost" => param("cor_cout"),
      "pf_comment" => param("cor_commentaire"),
      "pf_time" => param("cor_time"),
      "pf_comment" => param("cor_commentaire"),
      "is_deleted" => 'N',
    );

  # Reconstitution du correcteur avec les paramètres

  my $correcteur = schema->resultset('BppProofreader')->create(\%hashParamVerif);
  my $message = "Le correcteur ".$hashParam{"cor_nom"}." a été modifié.";

  var correcteur => $correcteur;
  var msgs => $message;

  vue_liste_correcteur();
};

### route '/corr/suppr' en GET
get '/corr/suppr/(?<id>\d+)'=>sub{
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};

  suppr_correcteur($id);
};

### route '/corr/suppr' en POST
post '/corr/suppr/(?<id>\d+)'=>sub{
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};

  my $message = "Le correcteur ".$id." a été ";
  my %hashParamVerif = (
      "is_deleted" => 'Y',
    );
  my $correcteur = schema->resultset('BppProofreader')->find($id);
  if ($correcteur != undef) {
    $correcteur->update(\%hashParamVerif);
    $message = $message."supprimé.";
  }

  vue_liste_correcteur();
};

# ------------------------------------------------------------
#  Gestion des revues
# ------------------------------------------------------------

### route '/revues' en GET
get '/revues' => sub {
  return redirect '/co' if !check_user();

  vue_liste_revue(); 
};

### route '/revues/view' en GET
get qr{/revues/view/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie
  
  my $res = schema->resultset('BppJournal')->search({'me.idbpp_journal' => $id});
  my $journalDetails = $res->first();

  my $resuuuu = schema->resultset('BppJourkey')->search({'me.idbpp_journal' => $id});
  my @publicationKeywords = $resuuuu->all();
  
  template 'revueviewdetails',{
  journalDetails => $journalDetails,
  publicationKeywords => \@publicationKeywords,
  }; 
};

### route '/add/revues' en GET
get '/revues/add' => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();

  my $rs = schema->resultset('BppKeyword')->search();
  my @keywords = $rs->all();

  template 'revueadd',{
    keywords=>\@keywords,
  };  
};

### route '/add/revues' en POST
post '/revues/add' => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  ## ------- Ajout de la revue -------
  # Création de la table des paramètres
  my %hashParamJournalAdd = ( "revue_titre"     => "title",
          "revue_theme" => "subject",
          "revue_template"   => "template_link",
          "revue_IF"   => "impact_factor", 
          );

  # A ajouter, vérification des paramètres
  
  # Génération table des paramètres
  my %hashParamVerifJournalAdd = (
      "title" => param("revue_titre"),
      "subject" => param("revue_theme"),
      "template_link" => param("revue_template"),
      "impact_factor" => param("revue_IF"),
      "is_deleted" => "N",
    );

  my $journalAjout = schema->resultset('BppJournal')->create(\%hashParamVerifJournalAdd);

  my $id_journal = $journalAjout->idbpp_journal;

  ## ------- Ajout des Keywords en relation avec le journal -------
  my $values  = param('revue_keyword');
  foreach (@$values){   
    # Création de la table des paramètres
    my %hashParamKeyword = ( "revue_keyword"     => "idbpp_keyword",
            );
    # A ajouter, vérification des paramètres
    # Génération table des paramètres
    my %hashParamVerifKeyword = (
        "idbpp_keyword" => $_,
        "idbpp_journal" => $id_journal,
      );

   # debug " param : ".param('revue_keyword')." id_journal = ".$id_journal." et la valeur en cours =".$_;

    my $keywordsJournal = schema->resultset('BppJourkey')->create(\%hashParamVerifKeyword);
  }

  vue_liste_revue();
};

### route '/revues/edit/:id' en GET
get qr{/revues/edit/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie

  #On récupère les données des revues autres que les mots clés
  my $rs = schema->resultset('BppJournal')->search({'me.idbpp_journal' => $id});
  my @revues = $rs->all();

  my $rs = schema->resultset('BppKeyword')->search();
  my @keywords = $rs->all();

  my $rs = schema->resultset('BppJourkey')->search({'me.idbpp_journal' => $id});
  my @journalKeywords = $rs->all();

  #On gère l'affichage dans la revue, ie on transmets à la vue :
  template 'revuemod',{
    revues => \@revues,
    keywords=>\@keywords,
    journalKeywords => \@journalKeywords,
  };  
};

post qr{/revues/edit/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie

  ## ------- Modification de la revue -------
  my $journalEdit = schema->resultset('BppJournal')->find($id);
  # Création de la table des paramètres
  my %hashParamJournalEdit = ( "revue_titre" => "title",
          "revue_theme" => "subject",
          "revue_template"   => "template_link",
          "revue_IF"   => "impact_factor", 
          );

  # A ajouter, vérification des paramètres
  
  # Génération table des paramètres
  my %hashParamVerifJournalEdit = (
      "title" => param("revue_titre"),
      "subject" => param("revue_theme"),
      "template_link" => param("revue_template"),
      "impact_factor" => param("revue_IF"),
    );

  $journalEdit->update(\%hashParamVerifJournalEdit);

  ## ------- Suppression des Keywords précédemment renseignés -------
  my $keywordsSup = schema->resultset('BppJourkey')->search({'me.idbpp_journal' => $id})->delete;
  ## ------- Ajout des Keywords en relation avec le journal -------
  my $values  = param('revue_keyword');
  foreach (@$values){   
    # Création de la table des paramètres
    my %hashParamKeyword = ( "revue_keyword"     => "idbpp_keyword",
            );
    # A ajouter, vérification des paramètres
    # Génération table des paramètres
    my %hashParamVerifKeyword = (
        "idbpp_keyword" => $_,
        "idbpp_journal" => $id,
      );
    my $publication = schema->resultset('BppJourkey')->create(\%hashParamVerifKeyword);
  }

  vue_liste_revue();
};

### route '/revues/suppr/:id' en GET
get qr{/revues/suppr/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie

  suppression_revue($id);

};

## route '/revues/suppr/:id' en POST
post qr{/revues/suppr/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
 my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie

  my $message = "La revue ".$id." a été ";
  my %hashParamVerif = (
      "is_deleted" => 'Y',
    );
  my $revue = schema->resultset('BppJournal')->find($id);
  if ($revue != undef) {
    $revue->update(\%hashParamVerif);
    $message = $message."supprimé.";
  }

  var msgs => $message;

  vue_liste_revue();
};

### route '/revues/recover/:id' en GET
get qr{/revues/recover/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie

  recover_revue($id);

};

### route '/revues/recover/:id' en POST
post qr{/revues/recover/(?<id>\d+)} => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();
  my $id = captures->{id}; # le paramètre id est dans l'URL
  delete params->{captures};
  params->{id} = $id; # on le recopie

  my $message = "La revue ".$id." a été ";
  my %hashParamVerif = (
      "is_deleted" => 'N',
    );
  my $revue = schema->resultset('BppJournal')->find($id);
  if ($revue != undef) {
    $revue->update(\%hashParamVerif);
    $message = $message."récupérée.";
  }

  var msgs => $message;

  vue_liste_revue();
};

# ------------------------------------------------------------
#  Gestion de l'extraction des données
# ------------------------------------------------------------

### route '/extract' en GET ou POST
any ['get', 'post'] => '/extract' => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();

  template 'extraction_possibilities';
};

### route '/extract/users' en GET
get qr{/extract/users} => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();

  extraction_users_go();

  return send_file($path.'/extraction_users.xls', system_path => 1, filename =>'extraction_users.xls');
};

### route '/extract/journaux' en GET
get qr{/extract/journaux} => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();

  extraction_journal_go();

  return send_file($path.'/extraction_journaux.xls', system_path => 1, filename =>'extraction_journaux.xls');
};

### route '/extract/correcteurs' en GET
get qr{/extract/correcteurs} => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();

  extraction_correcteur_go();

  return send_file($path.'/extraction_correcteur.xls', system_path => 1, filename =>'extraction_correcteur.xls');
};

### route '/extract/keywords' en GET
get qr{/extract/keywords} => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();

  extraction_keyword_go();

  return send_file($path.'/extraction_keyword.xls', system_path => 1, filename =>'extraction_keyword.xls');
};

### route '/extract/publications' en GET
get qr{/extract/publications} => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();

  extraction_publication_go();

  return send_file($path.'/extraction_publication.xls', system_path => 1, filename =>'extraction_publication.xls');
};

### route '/extract/all' en GET
get qr{/extract/all} => sub {
  return redirect '/co' if !check_user();
  return redirect '/' if !check_user_is_admin();

  extraction_totale_go();

  return send_file($path.'/a1_extraction_totale.xls', system_path => 1, filename =>'a1_extraction_totale.xls');
};

any qr{.*} => sub {
  status 'not_found';
  set layout => 'main';
  template '404', { path => request->path };
};

#++++++++++++++++++++++++++++++++++++++++
# +++++++ FONCTIONS +++++++
# +++++++ Les vues  +++++++
#++++++++++++++++++++++++++++++++++++++++

# ------------------------------------------------------------
#  User
# ------------------------------------------------------------

sub generate_password {
  my ($login, $password) = @_;
  return sha1_base64($login.$password);
}

sub generate_temp {
  my ($login, $date) = @_;
  return sha1_base64($login.$date);
}



sub check_password {
  my ($passwordtocheck, $login, $id, $hash) = @_;

  if (sha1_base64($id, $login, $passwordtocheck) == $hash)
  {
    return true;
  }
  else
  {
    return false;
  }
}

sub vue_liste_user {
  # --- Avons-nous des messages à afficher ?
  my $msgs = vars->{msgs};

  # --- On prépare la requête
  my $clause_where;
  my $attr;
  my $rs = schema->resultset('BppUser')->search();

  my @users = $rs->all();

  my $rs = schema->resultset('BppUsertype')->search();

  my @rights = $rs->all();

  my %hashRight = ();

  foreach my $user (@users)
  {
    $hashRight{$user->idbpp_user} = $user->bpp_usertype_idbpp_usertype->nom;
  }

  # --- On créé les données du template
  template 'usershow', {
    users => \@users,
    msgs => $msgs,
  };
}

sub vue_edit_user {
  # --- Avons-nous des messages à afficher ?
  my $id = $_[0];
  my $msgs = vars->{msgs};

  # --- On prépare la requête
  my $clause_where;
  my $attr;
  my $rs = schema->resultset('BppUser')->search({
    'me.is_deleted' => 'N',
    'me.idbpp_user' => $id,
    });

  my $user = $rs->first();

  my $rs = schema->resultset('BppUsertype')->search();

  my @rights = $rs->all();

  # --- On créé les données du template
  template 'useredit', {
    type => "modif",
    rights => \@rights,
    user => $user,
    msgs => $msgs,
  };
}

sub vue_add_user {
  # --- Avons-nous des messages à afficher ?
  my $id = $_[0];
  my $msgs = vars->{msgs} // {};

  # --- On prépare la requête
  my $clause_where;
  my $attr;

  my $rs = schema->resultset('BppUsertype')->search();

  my @rights = $rs->all();

  # --- On créé les données du template
  template 'useredit', {
    type => "ajout",
    rights => \@rights,
    msgs => $msgs,
  };
}

sub vue_delete_user {
  my $id = $_[0];
  my $msgs = vars->{msgs} // {};
  
  my $rs = schema->resultset('BppUser')->search({
    'me.is_deleted' => 'N',
    'me.idbpp_user' => $id,
    });

  my $user = $rs->first();

  template 'userdelete', {
    user => $user,
    msgs => $msgs,
  };
}

sub edit_user {
  my $href = shift;
  my %hashParam = %$href;
  # Modification ou insertion de l'utilisateur
  my $message;
  
  if ($hashParam{"user_password1"} eq $hashParam{"user_password2"})
  {
    my %hashParamVerif = (
      "name" => $hashParam{"user_name"},
      "surname" => $hashParam{"user_surname"},
      "epi" => $hashParam{"user_epi"},
      "email" => $hashParam{"user_email"},
      "position" => $hashParam{"user_role"},
      "bpp_usertype_idbpp_usertype" => $hashParam{"user_droit"},
      "is_deleted" => "N",
      );

    if ($hashParam{"user_password1"} ne "")
    {
      $hashParamVerif{"password"} = generate_password($hashParam{"user_login"}, $hashParam{"user_password1"});
    }

    my $user = schema->resultset('BppUser')->find($hashParam{"id"});
    if ($user == undef) {
      $user = schema->resultset('BppUser')->create(\%hashParamVerif);
      $message = $message."ajouté.";
      vue_liste_user();
    }
    else {
      $user->update(\%hashParamVerif);
      $message .= "Utilisateur modifié.";
      var msgs => $message;
      vue_liste_user();
    }
  }
  else
  {
    $message .= "Mot de passe différents";
    var msgs => $message;
    vue_edit_user($hashParam{"id"});
  }
}

sub edit_user_no_admin {
  my $href = shift;
  my %hashParam = %$href;
  # Modification ou insertion de l'utilisateur
  my $message;
  
  if ($hashParam{"user_password1"} eq $hashParam{"user_password2"})
  {
    my %hashParamVerif = (
      "name" => $hashParam{"user_name"},
      "surname" => $hashParam{"user_surname"},
      "epi" => $hashParam{"user_epi"},
      "email" => $hashParam{"user_email"},
      "position" => $hashParam{"user_role"},
      "is_deleted" => "N",
      );

    if ($hashParam{"user_password1"} ne "")
    {
      $hashParamVerif{"password"} = generate_password($hashParam{"user_login"}, $hashParam{"user_password1"});
    }

    my $user = schema->resultset('BppUser')->find($hashParam{"id"});
    if ($user == undef) {
      $user = schema->resultset('BppUser')->create(\%hashParamVerif);
      $message = $message."ajouté.";
      vue_liste_user();
    }
    else {
      $user->update(\%hashParamVerif);
      $message .= "Utilisateur modifié.";
      var msgs => $message;
      vue_liste_user();
    }
  }
  else
  {
    $message .= "Mot de passe différents";
    var msgs => $message;
    vue_edit_user($hashParam{"id"});
  }
}

sub recover_user {
  # --- Avons-nous des messages à afficher ?
  my $id = $_[0];
  my $msgs = vars->{msgs} // {};
  my $message;

  # Récupération

  my %hashParamVerif = (
    "is_deleted" => "N",
  );
  my $user = schema->resultset('BppUser')->find($id);
  if ($user == undef) {
    $message = $message."Utilisateur non récupéré (inexistant)";
    vue_liste_user();
  }
  else {
    $user->update(\%hashParamVerif);
    $message .= "Utilisateur récupéré.";
    var msgs => $message;
    vue_liste_user();
  }
}

sub add_user {
  my $href = shift;
  my %hashParam = %$href;
  # Modification ou insertion de l'utilisateur
  my $message;
  
  if ($hashParam{"user_password1"} eq $hashParam{"user_password2"})
  {
    my %hashParamVerif = (
      "name" => $hashParam{"user_name"},
      "surname" => $hashParam{"user_surname"},
      "login" => $hashParam{"user_login"},
      "epi" => $hashParam{"user_epi"},
      "email" => $hashParam{"user_email"},
      "position" => $hashParam{"user_role"},
      "bpp_usertype_idbpp_usertype" => $hashParam{"user_droit"},
      "is_deleted" => "N",
      );

    $hashParamVerif{"password"} = generate_password($hashParam{"user_login"}, $hashParam{"user_password1"});

    # On vérifie si l'utilisateur n'existe pas déjà
    my $verifUser = schema->resultset('BppUser')->search({'me.login' => $hashParam{"user_login"}});
    my $verifUserSortie = $verifUser->first();

    if ($verifUserSortie == undef) {
      my $user = schema->resultset('BppUser')->create(\%hashParamVerif);
      $message = $message."ajouté.";
      vue_liste_user();
    }
    else
    {
      $message .= "Login déjà utilisé";
      var msgs => $message;
      vue_add_user();
    }
  }
  else
  {
    $message .= "Mot de passe différents";
    var msgs => $message;
    vue_add_user();
  }
}

sub suppr_user {
  my $id = $_[0];
  my $msgs = vars->{msgs};
  
  my $message = "L'utilisateur ".$id." a été ";
  my %hashParamVerif = (
      "is_deleted" => 'Y',
    );
  my $user = schema->resultset('BppUser')->find($id);
  if ($user != undef) {
    $user->update(\%hashParamVerif);
    $message = $message."supprimé.";
  }

  vue_liste_user();
}

sub check_user {
  my $test = false;
  
  if (not session('user_id'))
  {
    return false;
  }
  else
  {
    my $user = recup_user_from_session();

    if ($user->is_deleted ne 'N')
    {
      return false;
    }
    return true;
  }
}

sub check_user_is_admin {
  my $user = recup_user_from_session();

  if ($user->bpp_usertype_idbpp_usertype->idbpp_usertype == 0)
  {
   return true;
  }
  return false;
}

sub check_user_is_referent {
  my $id = $_[0];
  my $user = recup_user_from_session();

  # --- On récupère les infos d ela table manage.
  my $resu = schema->resultset('BppManage')->search({'me.idbpp_publication' => $id});
  my $publicationManage = $resu->first();

  if ($publicationManage->idbpp_user->idbpp_user == $user->idbpp_user)
  {
   return true;
  }
  return false;
}

sub co_user {
  # user_login
  # user_password
  use DateTime;
  use DateTime::Format::HTTP;

  my $href = shift;
  my %hashParam = %$href;

  # Récupération de l'utilisateur matchant le login et le mot de passe
  my $rs = schema->resultset('BppUser')->search({
    'me.is_deleted' => 'N',
    'me.login' => $hashParam{"user_login"},
    'me.password' => generate_password($hashParam{"user_login"},$hashParam{"user_password"}),
  });

  my $user = $rs->first();
  my $test_newpass = 0;

  if (generate_password($hashParam{"user_login"},DateTime->now->ymd) eq $hashParam{"user_password"})
  {
    my $rs2 = schema->resultset('BppUser')->search({
      'me.is_deleted' => 'N',
      'me.login' => $hashParam{"user_login"},
      'me.bpp_usertype_idbpp_usertype' => ['0','1'], # Correspond aux utilisateurs ayant le droit de se connecter
    });

    $user = $rs2->first();
    $test_newpass = 1;
  }

  if ($user == undef) {
    # On renvoi la page d'erreur
    return vue_co_user_with_error($hashParam{"user_login"}, $hashParam{"user_login"});
  }
  else
  {
    # On renvoi à la page d'accueil
    session user_id => $user->idbpp_user;

    if ($test_newpass == "0")
    {
      return redirect '/pub/show';
    }
    else
    {
    return redirect '/profil';
    }
  }
}

sub password_recup {
  # TODO Envoi d'email.
  #    "user_login" => lc(param("identification")),
  #    "user_email" => param("email"),
  my $href = shift;
  my %hashParam = %$href;

  my $rs = schema->resultset('BppUser')->search({
    'me.login' => $hashParam{"user_login"},
    'me.email' => $hashParam{"user_email"},
    });

  my $user = $rs->first();

  if ($user == undef) {
    # On renvoi la page d'erreur
    return "Aucun couple identifiant/email correspondant";
  }
  else
  {
    # On renvoi à la page d'accueil
    use MIME::Lite::TT::HTML;
    use DateTime;
    use DateTime::Format::HTTP;

    my $dt_now = DateTime->now->ymd;
    my %options;
    my %params;
    $options{INCLUDE_PATH} = $cfg->param("site.pathtemplatemail");
    my $email_from = $cfg->param("email.from");
    $params{dt_now} = $dt_now;
    $params{login} =  $hashParam{"user_login"};
    $params{temp_pass} = generate_temp($hashParam{"user_login"},$dt_now);
    my $msg = MIME::Lite::TT::HTML->new(
                From        =>  $email_from,
                To          =>  $hashParam{"user_email"},
                Subject     =>  '[Gestion Publication] Récupération mot de passe',
                Template    =>  {
                                    text    =>  'recuppass.tt',
                                    html    =>  'recuppass.html.tt',
                                },
                TmplOptions =>  \%options,
                TmplParams  =>  \%params,
    );

    # Require sendmail
    $msg->send();
    return "Un mot de passe temporaire vous a été envoyé (actif pour la journée).";
  }

}

sub recup_user_from_session {
  my $id = session 'user_id';
  my $rs = schema->resultset('BppUser')->search({
    'me.idbpp_user' => $id,
    });


  return $rs->first();
}

sub vue_co_user_with_error {
  set layout => 'main';
  template 'index', {
    error => true,
  };
}

# ------------------------------------------------------------
#  Correcteur
# ------------------------------------------------------------

sub vue_liste_correcteur {

  # --- Avons-nous des messages à afficher ?
  my $msgs = vars->{msgs};

  # --- On prépare la requête
  my $clause_where;
  my $attr;
  my $rs = schema->resultset('BppProofreader')->search();

  my @correcteurs = $rs->all();

  # --- On créé les données du template
  template 'corr', {
    correcteurs => \@correcteurs,
    msgs => $msgs,
  };
}

sub edit_correcteur {
  my $id = $_[0];
  my $msgs = vars->{msgs};
  
  my $rs = schema->resultset('BppProofreader')->search({
    'me.is_deleted' => 'N',
    'me.idbpp_proofreader' => $id,
    });

  my $correcteur = $rs->first();

  template 'corredit', {
    type => "modif",
    correcteur => $correcteur,
    msgs => $msgs,
  };
}

sub suppr_correcteur {
  my $id = $_[0];
  my $msgs = vars->{msgs};
  
  my $rs = schema->resultset('BppProofreader')->search({
    'me.is_deleted' => 'N',
    'me.idbpp_proofreader' => $id,
    });

  my $correcteur = $rs->first();

  template 'corrdelete', {
    correcteur => $correcteur,
    msgs => $msgs,
  };
}

sub recover_corr {
  # --- Avons-nous des messages à afficher ?
  my $id = $_[0];
  my $msgs = vars->{msgs};
  my $message;

  # Récupération

  my %hashParamVerif = (
    "is_deleted" => "N",
  );
  my $user = schema->resultset('BppProofreader')->find($id);
  if ($user == undef) {
    $message = $message."Correcteur non récupéré (inexistant)";
    vue_liste_correcteur();
  }
  else {
    $user->update(\%hashParamVerif);
    $message .= "Correcteur récupéré.";
    var msgs => $message;
    vue_liste_correcteur();
  }

}

# ------------------------------------------------------------
#  Publication
# ------------------------------------------------------------

sub lancement_ajout_publication {

  # --- On prépare la requête
  my $clause_where;
  my $attr;
  my $rs = schema->resultset('BppJournal')->search({'me.is_deleted' => 'N'});

  # -- On récupère toutes les publications
  my @journaux = $rs->all();


  # --- On prépare la requête
  my $clause_where;
  my $attr;
  my $res = schema->resultset('BppUser')->search({'me.is_deleted' => 'N'});

  # -- On récupère tous les utilisateurs
  my @utilisateurs = $res->all();

  # --- On prépare la requête
  my $clause_where;
  my $attr;
  my $rees = schema->resultset('BppKeyword')->search({'me.is_deleted' => 'N'});

  # -- On récupère tous les mots clés
  my @keywords = $rees->all();

  # --- On prépare la requête
  my $clause_where;
  my $attr;
  my $reees = schema->resultset('BppProofreader')->search({'me.is_deleted' => 'N'});

  # -- On récupère tous les mots clés
  my @correcteurs = $reees->all();

  my $today = DateTime->now->strftime('%Y-%m-%d');

  my $year= substr $today, 0, 4;
  my $month= substr $today, 5, 2;
  my $day= substr $today, 8, 2;

  my $aweeklater= DateTime->now->add( days => 7 );

  $aweeklater= substr $aweeklater, 0, 10;

  debug "semaine plus tard :".$aweeklater." et sinon on est le ".$today;

  # --- On créé les données du template
  template 'pubadd', {
    journaux => \@journaux,
    utilisateurs => \@utilisateurs,
    keywords => \@keywords,
    correcteurs => \@correcteurs,
    today => $today,
    aweeklater => $aweeklater,
  };

}

sub ajout_de_la_publication {

my $params = request->params;

## ------- Ajout de la publication -------
  # Création de la table des paramètres
  my %hashParamPubTab = ( "title"     => "title",
          "theme" => "subject",
          "idea"   => "main_ideas",
          "resume"   => "abstract",
          "revue"   => "idbpp_journal", 
          );

  # A ajouter, vérification des paramètres
  
  # Génération table des paramètres
  my %hashParamVerifPubTab = (
      "idbpp_journal" => param("revue"),
      "title" => param("title"),
      "subject" => param("theme"),
      "main_ideas" => param("idea"),
      "abstract" => param("resume"),
      "is_deleted" => "N",
    );

  my $publication = schema->resultset('BppPublication')->create(\%hashParamVerifPubTab);

  # Récupération de l'id de la publication :
  my $id_publi = $publication->idbpp_publication;

## ------- Ajout de l'auteur principal -------
  # Création de la table des paramètres
  my %hashParamManageTab1 = ( "autor"     => "idbpp_user",
          "autorinfo"   => "function", 
          );

  # A ajouter, vérification des paramètres
  
  # Génération table des paramètres
  my %hashParamVerifManageTab1 = (
      "idbpp_user" => param("autor"),
      "idbpp_publication" => $id_publi,
      "rank" => 1,
      "function" => param("autorinfo"),
    );

  my $autor1 = schema->resultset('BppManage')->create(\%hashParamVerifManageTab1);

## ------- Ajout des autres auteurs -------
  my $i = 1;
  my $testAutor = 'autor_sec_'.$i;
  while (param($testAutor))
  { 
    # Création de la table des paramètres
    my %hashParamManageTab2 = ( $testAutor  => "idbpp_user",
            'autor_sec_' . $i . '_info'   => "function", 
            );

    # A ajouter, vérification des paramètres
    
    # Génération table des paramètres
    my %hashParamVerifManageTab2 = (
        "idbpp_user" => param($testAutor),
        "idbpp_publication" => $id_publi,
        "rank" => $i+1,
        "function" => param('autor_sec_' . $i . '_info'),
      );

    my $autorn = schema->resultset('BppManage')->create(\%hashParamVerifManageTab2);
    $i++;
    $testAutor='autor_sec_'.$i;
  }

## ------- Ajout des Keywords en relation avec la publication -------
my $values  = param('keywords');
foreach (@$values){
    
  # Création de la table des paramètres
  my %hashParamKeyword = ( "keywords"     => "idbpp_keyword",
          );

  # A ajouter, vérification des paramètres

  # Génération table des paramètres
  my %hashParamVerifKeyword = (
      "idbpp_keyword" => $_,
      "idbpp_publication" => $id_publi,
    );

  my $publication = schema->resultset('BppPubkey')->create(\%hashParamVerifKeyword);

}

## ------- Ajout des informations concernant le correcteur de la publication -------
if (param("corrector")!=0)
{
  # Création de la table des paramètres
  my %hashParamManageCorr = ( "typecorrector"     => "proofreading_status",
          "corrector"   => "idbpp_proofreader", 
          );

  # A ajouter, vérification des paramètres
  
  # Génération table des paramètres
  my %hashParamVerifCorr = (
      "proofreading_status" => param("typecorrector"),
      "idbpp_publication" => $id_publi,
      "idbpp_proofreader" => param("corrector"),
    );

  my $corrector = schema->resultset('BppProofreading')->create(\%hashParamVerifCorr);
}

## ------- Ajout des status correspondant à la publication -------
$i = 1;
my $testStatus = 'histoname_'.$i;
my $messageError = 'No';
while (param($testStatus))
{ 
  my $selectedstat="";
  
  if (param('selectedstatut_'.$i)==$i)
  {
    $selectedstat="Y";
  }
  else
  {
    $selectedstat="N";
  }
  
  # debug " param : ".param($testStatus)." i = ".$i." et donc selectedstat =".$selectedstat;

   # Création de la table des paramètres
    my %hashParamManageStat1 = ( $testStatus     => "type",
            'datedebpre_'.$i     => "beginning_date",
            'datefinpre_'.$i     => "estimated_date",
            'datefinrel_'.$i   => "final_date", 
            'histo'.$i   => "status_detail",
            'selectedstatut_'.$i  => "status_actuel", 
            );

    # A ajouter, vérification des paramètres

    # Génération table des paramètres
    my %hashParamVerifStat1 = (
        "bpp_publication_idbpp_publication" => $id_publi,
        "type" => param($testStatus),
        "beginning_date" => param('datedebpre_'.$i),
        "estimated_date" => param('datefinpre_'.$i),
        "final_date" => param('datefinrel_'.$i),
        "status_detail" => param('histo'.$i),
        "status_actuel" => $selectedstat,
        "status_is_deleted" => "N",
      );

    my $redactionstatus = schema->resultset('BppStatus')->create(\%hashParamVerifStat1);

  $i++;
  $testStatus = 'histoname_'.$i;
}

## ------- Affichage du message de validation de l'ajout -------
  $i--;
  my $message = 'La publication ayant pour titre : ' . $hashParamVerifPubTab{"title"} . ' et pour id ' . $id_publi . ' a été ajouté. Cette publication contenait : ' . $i . 'statut';

  var msgs => $message;

  vue_publication_modif();
}

sub lancement_edit_publication {

  my $id = $_[0];

  # --- On prépare la requête
  my $rs = schema->resultset('BppJournal')->search({'me.is_deleted' => 'N'});
  # -- On récupère toutes les publications
  my @journaux = $rs->all();


  # --- On prépare la requête
  my $res = schema->resultset('BppUser')->search({'me.is_deleted' => 'N'});
  # -- On récupère tous les utilisateurs
  my @utilisateurs = $res->all();


  # --- On prépare la requête
  my $rees = schema->resultset('BppKeyword')->search({'me.is_deleted' => 'N'});
  # -- On récupère tous les mots clés
  my @keywords = $rees->all();


  # --- On prépare la requête
  my $reees = schema->resultset('BppProofreader')->search({'me.is_deleted' => 'N'});
  # -- On récupère tous les mots clés
  my @correcteurs = $reees->all();

  # -- On récupère la publication concernée
  my $publication = schema->resultset('BppPublication')->find($id);

  # --- On prépare la requête
  my $resu = schema->resultset('BppManage')->search({'me.idbpp_publication' => $id});
  # -- On récupère tous les auteurs de la publication concernée
  my @publicationUsersManage = $resu->all();

  # --- On prépare la requête
  my $resuu = schema->resultset('BppProofreading')->search({'me.idbpp_publication' => $id});
  # -- On récupère tous les auteurs de la publication concernée
  my @publicationCorrecteurs = $resuu->all();

  # --- On prépare la requête
  my $resuuu = schema->resultset('BppStatus')->search({'me.bpp_publication_idbpp_publication' => $id,'me.status_is_deleted' => "N"});
  # -- On récupère tous les auteurs de la publication concernée
  my @publicationStatus = $resuuu->all();

  # --- On prépare la requête
  my $resuuuu = schema->resultset('BppPubkey')->search({'me.idbpp_publication' => $id});
  # -- On récupère tous les auteurs de la publication concernée
  my @publicationKeywords = $resuuuu->all();

  # --- On créé les données du template
  template 'pubedit', {
    journaux => \@journaux,
    utilisateurs => \@utilisateurs,
    keywords => \@keywords,
    correcteurs => \@correcteurs,
    id => $id,
    publication => $publication,
    publicationUsersManage => \@publicationUsersManage,
    publicationKeywords => \@publicationKeywords,
    publicationCorrecteurs => \@publicationCorrecteurs,
    publicationStatus => \@publicationStatus,
  };
}

sub edition_de_la_publication{
  # Récupération de l'id de la publication :
  my $id_publi = $_[0];

## ------- Modification de la publication -------
  my $publication = schema->resultset('BppPublication')->find($id_publi);
  # Création de la table des paramètres
  my %hashParamPubTab = ( "title"     => "title",
          "theme" => "subject",
          "idea"   => "main_ideas",
          "resume"   => "abstract",
          "revue"   => "idbpp_journal", 
          );

  # A ajouter, vérification des paramètres
  
  # Génération table des paramètres
  my %hashParamVerifPubTab = (
      "idbpp_journal" => param("revue"),
      "title" => param("title"),
      "subject" => param("theme"),
      "main_ideas" => param("idea"),
      "abstract" => param("resume"),
      "is_deleted" => "N",
    );

  $publication->update(\%hashParamVerifPubTab);

## ------- Suppression des auteurs précédemment renseignés -------
  my $autor1=schema->resultset('BppManage')->search({'me.idbpp_publication' => $id_publi})->delete;

## ------- Ajout du nouvel auteur principal -------
  # # ------- selection de l'auteur principal precedemment renseigné -------
  # my $autor1=schema->resultset('BppManage')->search({'me.idbpp_publication' => $id_publi,'me.rank' => 1});
  # # ------- suppression de l'auteur principal precedemment renseigné -------
  # $autor1->delete;
  # ------- ajout du nouvel auteur principal -------
  # Création de la table des paramètres
  my %hashParamManageTab1 = ( "autor"     => "idbpp_user",
          "autorinfo"   => "function", 
          );

  # A ajouter, vérification des paramètres
  
  # Génération table des paramètres
  my %hashParamVerifManageTab1 = (
      "idbpp_user" => param("autor"),
      "idbpp_publication" => $id_publi,
      "rank" => 1,
      "function" => param("autorinfo"),
    );

  my $autor2 = schema->resultset('BppManage')->create(\%hashParamVerifManageTab1);

## ------- Ajout des autres auteurs -------
  my $i = 1;
  my $testAutor = 'autor_sec_'.$i;
  while (param($testAutor))
  { 
    # ------- ajout du nouvel auteur -------
    # Création de la table des paramètres
    my %hashParamManageTab2 = ( $testAutor  => "idbpp_user",
            'autor_sec_' . $i . '_info'   => "function", 
            );

    # A ajouter, vérification des paramètres
    
    # Génération table des paramètres
    my %hashParamVerifManageTab2 = (
        "idbpp_user" => param($testAutor),
        "idbpp_publication" => $id_publi,
        "rank" => $i+1,
        "function" => param('autor_sec_' . $i . '_info'),
      );

    my $autorn = schema->resultset('BppManage')->create(\%hashParamVerifManageTab2);
    $i++;
    $testAutor='autor_sec_'.$i;
  }

## ------- Modification des Keywords en relation avec la publication -------
 # ------- Suppression des Keywords précédemment renseignés -------
  my $keywordsSup = schema->resultset('BppPubkey')->search({'me.idbpp_publication' => $id_publi})->delete;
 # ------- Ajout des Keywords en relation avec la publication -------
my $values  = param('keywords');
foreach (@$values){

  # Création de la table des paramètres
  my %hashParamKeyword = ( "keywords"     => "idbpp_keyword",
          );

  # A ajouter, vérification des paramètres

  # Génération table des paramètres
  my %hashParamVerifKeyword = (
      "idbpp_keyword" => $_,
      "idbpp_publication" => $id_publi,
    );

  my $keywordspub = schema->resultset('BppPubkey')->create(\%hashParamVerifKeyword);
}

## ------- Modification des informations concernant le correcteur de la publication -------
  # ------- Suppression des infos concernant le correcteur précédemment renseigné -------
  my $correctorsup=schema->resultset('BppProofreading')->search({'me.idbpp_publication' => $id_publi})->delete;
  # ------- Ajout des nouvelles informations sur le correcteur -------
  if (param("corrector")!=0)
  {
    # Création de la table des paramètres
    my %hashParamManageCorr = ( "typecorrector"     => "proofreading_status",
            "corrector"   => "idbpp_proofreader", 
            );

    # A ajouter, vérification des paramètres
    
    # Génération table des paramètres
    my %hashParamVerifCorr = (
        "proofreading_status" => param("typecorrector"),
        "idbpp_publication" => $id_publi,
        "idbpp_proofreader" => param("corrector"),
      );

    my $corrector = schema->resultset('BppProofreading')->create(\%hashParamVerifCorr);
  }

## ------- Modification des status correspondant à la publication -------
# ------- Suppression des infos concernant les status précédemment renseigné -------
  my @statusdel=schema->resultset('BppStatus')->search({'me.bpp_publication_idbpp_publication' => $id_publi})->update({'status_is_deleted' => "Y"});

  # ------- Ajout des nouvelles informations sur le satut -------
  $i = 1;
  my $testStatus = 'histoname_'.$i;
  my $messageError = 'No';
  while (param($testStatus))
  { 

    my $selectedstat="";
    
    if (param('selectedstatut_'.$i)==$i)
    {
      $selectedstat="Y";
    }
    else
    {
      $selectedstat="N";
    }
  
     # Création de la table des paramètres
      my %hashParamManageStat1 = ( $testStatus     => "type",
              'datedebpre_'.$i     => "beginning_date",
              'datefinpre_'.$i     => "estimated_date",
              'datefinrel_'.$i   => "final_date", 
              'histo'.$i   => "status_detail",
              'selectedstatut_'.$i   => "status_actuel", 
              );

      # A ajouter, vérification des paramètres
      
      # Génération table des paramètres
      my %hashParamVerifStat1 = (
        "bpp_publication_idbpp_publication" => $id_publi,
        "type" => param($testStatus),
        "beginning_date" => param('datedebpre_'.$i),
        "estimated_date" => param('datefinpre_'.$i),
        "final_date" => param('datefinrel_'.$i),
        "status_detail" => param('histo'.$i),
        "status_actuel" => $selectedstat,
        "status_is_deleted" => "N",
        );

      my $redactionstatus = schema->resultset('BppStatus')->create(\%hashParamVerifStat1);

    $i++;
    $testStatus = 'histoname_'.$i;
  }

## ------- Affichage du message de validation de l'ajout -------
  $i--;
  my $message = 'La publication ayant pour titre : ' . $hashParamVerifPubTab{"title"} . ' et pour id ' . $id_publi . ' a été ajouté. Cette publication contenait : ' . $i . 'auteurs';

  var msgs => $message;

  vue_publication_modif();
}

sub lancement_duplication_publication{

  my $id = $_[0];

  # --- On prépare la requête
  my $rs = schema->resultset('BppJournal')->search({'me.is_deleted' => 'N'});
  # -- On récupère toutes les publications
  my @journaux = $rs->all();


  # --- On prépare la requête
  my $res = schema->resultset('BppUser')->search({'me.is_deleted' => 'N'});
  # -- On récupère tous les utilisateurs
  my @utilisateurs = $res->all();


  # --- On prépare la requête
  my $rees = schema->resultset('BppKeyword')->search({'me.is_deleted' => 'N'});
  # -- On récupère tous les mots clés
  my @keywords = $rees->all();


  # --- On prépare la requête
  my $reees = schema->resultset('BppProofreader')->search({'me.is_deleted' => 'N'});
  # -- On récupère tous les mots clés
  my @correcteurs = $reees->all();

  # -- On récupère la publication concernée
  my $publication = schema->resultset('BppPublication')->find($id);

  # --- On prépare la requête
  my $resu = schema->resultset('BppManage')->search({'me.idbpp_publication' => $id});
  # -- On récupère tous les auteurs de la publication concernée
  my @publicationUsersManage = $resu->all();

  # --- On prépare la requête
  my $resuu = schema->resultset('BppProofreading')->search({'me.idbpp_publication' => $id});
  # -- On récupère tous les auteurs de la publication concernée
  my @publicationCorrecteurs = $resuu->all();

  # --- On prépare la requête
  my $resuuu = schema->resultset('BppStatus')->search({'me.bpp_publication_idbpp_publication' => $id,'me.status_is_deleted' => "N"});
  # -- On récupère tous les auteurs de la publication concernée
  my @publicationStatus = $resuuu->all();

  # --- On prépare la requête
  my $resuuuu = schema->resultset('BppPubkey')->search({'me.idbpp_publication' => $id});
  # -- On récupère tous les auteurs de la publication concernée
  my @publicationKeywords = $resuuuu->all();

  # --- On créé les données du template
  template 'pubduplicate', {
    journaux => \@journaux,
    utilisateurs => \@utilisateurs,
    keywords => \@keywords,
    correcteurs => \@correcteurs,
    id => $id,
    publication => $publication,
    publicationUsersManage => \@publicationUsersManage,
    publicationKeywords => \@publicationKeywords,
    publicationCorrecteurs => \@publicationCorrecteurs,
    publicationStatus => \@publicationStatus,
  };
}

sub duplication_de_la_publication {

my $params = request->params;

## ------- Ajout de la publication -------
  # Création de la table des paramètres
  my %hashParamPubTab = ( "title"     => "title",
          "theme" => "subject",
          "idea"   => "main_ideas",
          "resume"   => "abstract",
          "revue"   => "idbpp_journal", 
          );

  # A ajouter, vérification des paramètres
  
  # Génération table des paramètres
  my %hashParamVerifPubTab = (
      "idbpp_journal" => param("revue"),
      "title" => param("title"),
      "subject" => param("theme"),
      "main_ideas" => param("idea"),
      "abstract" => param("resume"),
      "is_deleted" => "N",
    );

  my $publication = schema->resultset('BppPublication')->create(\%hashParamVerifPubTab);

  # Récupération de l'id de la publication :
  my $id_publi = $publication->idbpp_publication;

## ------- Ajout de l'auteur principal -------
  # Création de la table des paramètres
  my %hashParamManageTab1 = ( "autor"     => "idbpp_user",
          "autorinfo"   => "function", 
          );

  # A ajouter, vérification des paramètres
  
  # Génération table des paramètres
  my %hashParamVerifManageTab1 = (
      "idbpp_user" => param("autor"),
      "idbpp_publication" => $id_publi,
      "rank" => 1,
      "function" => param("autorinfo"),
    );

  my $autor1 = schema->resultset('BppManage')->create(\%hashParamVerifManageTab1);

## ------- Ajout des autres auteurs -------
  my $i = 1;
  my $testAutor = 'autor_sec_'.$i;
  while (param($testAutor))
  { 
    # Création de la table des paramètres
    my %hashParamManageTab2 = ( $testAutor  => "idbpp_user",
            'autor_sec_' . $i . '_info'   => "function", 
            );

    # A ajouter, vérification des paramètres
    
    # Génération table des paramètres
    my %hashParamVerifManageTab2 = (
        "idbpp_user" => param($testAutor),
        "idbpp_publication" => $id_publi,
        "rank" => $i+1,
        "function" => param('autor_sec_' . $i . '_info'),
      );

    my $autorn = schema->resultset('BppManage')->create(\%hashParamVerifManageTab2);
    $i++;
    $testAutor='autor_sec_'.$i;
  }

## ------- Ajout des Keywords en relation avec la publication -------
my $values  = param('keywords');
foreach (@$values){
    
  # Création de la table des paramètres
  my %hashParamKeyword = ( "keywords"     => "idbpp_keyword",
          );

  # A ajouter, vérification des paramètres

  # Génération table des paramètres
  my %hashParamVerifKeyword = (
      "idbpp_keyword" => $_,
      "idbpp_publication" => $id_publi,
    );

  my $publication = schema->resultset('BppPubkey')->create(\%hashParamVerifKeyword);

}

## ------- Ajout des informations concernant le correcteur de la publication -------
if (param("corrector")!=0)
{
  # Création de la table des paramètres
  my %hashParamManageCorr = ( "typecorrector"     => "proofreading_status",
          "corrector"   => "idbpp_proofreader", 
          );

  # A ajouter, vérification des paramètres
  
  # Génération table des paramètres
  my %hashParamVerifCorr = (
      "proofreading_status" => param("typecorrector"),
      "idbpp_publication" => $id_publi,
      "idbpp_proofreader" => param("corrector"),
    );

  my $corrector = schema->resultset('BppProofreading')->create(\%hashParamVerifCorr);
}

## ------- Ajout des status correspondant à la publication -------
$i = 1;
my $testStatus = 'histoname_'.$i;
my $messageError = 'No';
while (param($testStatus))
{ 
  my $selectedstat="";
  
  if (param('selectedstatut_'.$i)==$i)
  {
    $selectedstat="Y";
  }
  else
  {
    $selectedstat="N";
  }
  
  # debug " param : ".param($testStatus)." i = ".$i." et donc selectedstat =".$selectedstat;

   # Création de la table des paramètres
    my %hashParamManageStat1 = ( $testStatus     => "type",
            'datedebpre_'.$i     => "beginning_date",
            'datefinpre_'.$i     => "estimated_date",
            'datefinrel_'.$i   => "final_date", 
            'histo'.$i   => "status_detail",
            'selectedstatut_'.$i   => "status_actuel", 
            );

    # A ajouter, vérification des paramètres

    # Génération table des paramètres
    my %hashParamVerifStat1 = (
        "bpp_publication_idbpp_publication" => $id_publi,
        "type" => param($testStatus),
        "beginning_date" => param('datedebpre_'.$i),
        "estimated_date" => param('datefinpre_'.$i),
        "final_date" => param('datefinrel_'.$i),
        "status_detail" => param('histo'.$i),
        "status_actuel" => $selectedstat,
        "status_is_deleted" => "N",
      );

    my $redactionstatus = schema->resultset('BppStatus')->create(\%hashParamVerifStat1);

  $i++;
  $testStatus = 'histoname_'.$i;
}

## ------- Affichage du message de validation de l'ajout -------
  $i--;
  my $message = 'La publication ayant pour titre : ' . $hashParamVerifPubTab{"title"} . ' et pour id ' . $id_publi . ' a été ajouté. Cette publication contenait : ' . $i . 'statut';

  var msgs => $message;

  vue_publication_modif();
}

sub vue_publication_modif {

  if (check_user_is_admin()) {
    vue_publication_admin();
  }
  else{
    vue_publication_perso();
  }

}

sub vue_publication_perso {

  # On récupère l'utilisateur actuellement connecté
  my $userconnected = recup_user_from_session();
  my $userid = $userconnected->idbpp_user;

  # On récupère les id des publications le concernant
  my $res = schema->resultset('BppManage')->search();
  my @publicationsmanage = $res->all();

  my $res = schema->resultset('BppStatus')->search({'me.status_actuel' => "Y",'me.status_is_deleted' => "N"});
  my @status = $res->all();

  template 'pubshow',{
  userconnected => $userconnected,
  publicationsmanage => \@publicationsmanage,
  status => \@status,
  }; 

}

sub vue_publication_details {

  my $id = $_[0];

  # --- On prépare la requête
  my $rees = schema->resultset('BppKeyword')->search({'me.is_deleted' => 'N'});
  # -- On récupère tous les mots clés
  my @keywords = $rees->all();

  # -- On récupère la publication concernée
  my $publication = schema->resultset('BppPublication')->find($id);

  # --- On prépare la requête
  my $resu = schema->resultset('BppManage')->search({'me.idbpp_publication' => $id},{order_by => 'me.rank'});
  # -- On récupère tous les auteurs de la publication concernée
  my @publicationUsersManage = $resu->all();

  # --- On prépare la requête
  my $resuu = schema->resultset('BppProofreading')->search({'me.idbpp_publication' => $id});
  # -- On récupère tous les auteurs de la publication concernée
  my @publicationCorrecteurs = $resuu->all();

  # --- On prépare la requête
  my $resuuu = schema->resultset('BppStatus')->search({'me.bpp_publication_idbpp_publication' => $id,'me.status_is_deleted' => "N"});
  # -- On récupère tous les auteurs de la publication concernée
  my @publicationStatus = $resuuu->all();

  # --- On prépare la requête
  my $resuuuu = schema->resultset('BppPubkey')->search({'me.idbpp_publication' => $id});
  # -- On récupère tous les auteurs de la publication concernée
  my @keywords = $resuuuu->all();

  # --- On créé les données du template
  template 'pubviewdetails', {
    id => $id,
    publication => $publication,
    keywords => \@keywords,
    publicationUsersManage => \@publicationUsersManage,
    publicationCorrecteurs => \@publicationCorrecteurs,
    publicationStatus => \@publicationStatus,
  };
}

sub suppr_publication {
  my $id = $_[0];
  my $msgs = vars->{msgs};
  
  my $rs = schema->resultset('BppPublication')->search({
    'me.is_deleted' => 'N',
    'me.idbpp_publication' => $id,
    });

  my $publication = $rs->first();

  my $idpubli = $publication->idbpp_publication;

  # --- On prépare la requête
  my $res = schema->resultset('BppStatus')->search({'me.bpp_publication_idbpp_publication' => $id,'me.status_is_deleted' => "N"});
  # -- On récupère toutes les publications
  my @status = $res->all();

  template 'publidelete', {
    publication => $publication,
    status => \@status,
    msgs => $msgs,
  };
}

sub recover_publication {
  my $id = $_[0];
  my $msgs = vars->{msgs};
  
  my $rs = schema->resultset('BppPublication')->search({
    'me.is_deleted' => 'Y',
    'me.idbpp_publication' => $id,
    });

  my $publication = $rs->first();

  my $idpubli = $publication->idbpp_publication;

  # --- On prépare la requête
  my $res = schema->resultset('BppStatus')->search({'me.bpp_publication_idbpp_publication' => $id,'me.status_is_deleted' => "N"});
  # -- On récupère toutes les publications
  my @status = $res->all();

  template 'publirecover', {
    publication => $publication,
    status => \@status,
    msgs => $msgs,
  };
}

sub vue_publication_admin {

  my $res = schema->resultset('BppPublication')->search({},{order_by => 'me.title'});
  my @publications = $res->all();

  my $resu = schema->resultset('BppManage')->search();
  my @publicationUsersManage = $resu->all();

  my $res = schema->resultset('BppStatus')->search({'me.status_actuel' => "Y",'me.status_is_deleted' => "N"});
  my @status = $res->all();

  # --- On créé les données du template
  template 'pubshowadmin', {
    publications => \@publications,
    publicationUsersManage => \@publicationUsersManage,
    status => \@status,
  };
}

# ------------------------------------------------------------
#  Revues
# ------------------------------------------------------------

sub vue_liste_revue {
  #On récupère les données des revues
  my $rs = schema->resultset('BppJournal')->search({},{order_by => 'me.title'});
  my @revues = $rs->all();

  my $msgs = vars->{msgs};

  template 'revueshow',{
  revues => \@revues,
  msgs => $msgs,
  }; 
}

sub suppression_revue {
  my $id = $_[0];
  my $msgs = vars->{msgs};
  
  my $res = schema->resultset('BppJournal')->search({'me.idbpp_journal' => $id,'me.is_deleted' => 'N'});

  my $journalSup = $res->first();

  # my $idjournal = $journalSup->idbpp_journal;

  template 'revuedelete', {
    journalSup => $journalSup,
    msgs => $msgs,
  };
}

sub recover_revue {
  my $id = $_[0];
  my $msgs = vars->{msgs};
  
  my $rs = schema->resultset('BppJournal')->search({
    'me.is_deleted' => 'Y',
    'me.idbpp_journal' => $id,
    });

  my $journalRecup = $rs->first();

  my $idjournal = $journalRecup->idbpp_journal;

  template 'revuerecover', {
    journalRecup => $journalRecup,
    msgs => $msgs,
  };
}

# ------------------------------------------------------------
#  Mots clés
# ------------------------------------------------------------

sub add_keyword {
  my $motcletexte = $_[0];
  my $message;
  my $sortie;

  if ($motcletexte ne "")
  {
    my %hashParamVerifKeyWordTab = (
      "word" => $motcletexte,
      "is_deleted" => "N",
    );

    # On vérifie si l'utilisateur n'existe pas déjà
    my $verifKeyword = schema->resultset('BppKeyword')->search({'me.word' => $motcletexte});
    my $verifKeywordSortie = $verifKeyword->first();

    if ($verifKeywordSortie == undef) {
      # Mot clé inexistant
      my $keyword = schema->resultset('BppKeyword')->create(\%hashParamVerifKeyWordTab);
      $message = $message.$motcletexte." ajouté.";

      $sortie = $keyword->idbpp_keyword;
    }
    else
    {
      $message .= "Mot clé déjà existant";
      $sortie = $verifKeywordSortie->idbpp_keyword;
    }
  }
  else
  {
    $message .= "Mot clé saisi nul";
    $sortie = -1;
  }

  var msgs => $message;

  return $sortie;
}

sub vue_liste_keyword {

# --- Avons-nous des messages à afficher ?
  my $msgs = vars->{msgs};
  my $val;
  my %tabledehachage = ();

  # --- On prépare la requête
  my $rs = schema->resultset('BppKeyword')->search({'me.is_deleted' => 'N'},{order_by => 'me.word'});

  my @keywords = $rs->all();

  for (@keywords){
    $tabledehachage{$_->idbpp_keyword}=get_usage_keyword($_->idbpp_keyword);
  }

  # --- On créé les données du template
  template 'keywordview', {
    keywords => \@keywords,
    number_kw => \%tabledehachage,
    msgs => $msgs,
  };
}

sub get_usage_keyword {
  my $test = $_[0];
  my $rs_revue = schema->resultset('BppJourkey')->search({'me.idbpp_keyword' => $test});
  my @revue_link = $rs_revue->all();
  my $rs_publication = schema->resultset('BppPubkey')->search({'me.idbpp_keyword' => $test});
  my @publication_link = $rs_publication->all();
  my $val = 0;

  for my $temp_revue (@revue_link){
    if ($temp_revue->idbpp_journal->is_deleted eq "N") {
      $val++;
    }
  }

  for my $temp_publication (@publication_link){
    if ($temp_publication->idbpp_publication->is_deleted eq "N") {
      $val++;
    }
  }

  return $val;
}

sub vue_contenu_keyword {
  my $id = $_[0];
  my $msgs = "";

  my $rs = schema->resultset('BppKeyword')->search({'me.idbpp_keyword' => $id});
  my $keyword = $rs->first();

  my @keywords = $rs->all();
  my $rs_revue = schema->resultset('BppJourkey')->search({'me.idbpp_keyword' => $id});
  my @revue_link = $rs_revue->all();
  my $rs_publication = schema->resultset('BppPubkey')->search({'me.idbpp_keyword' => $id});
  my @publication_link = $rs_publication->all();

  template 'keywordcontent', {
    keyword => $keyword,
    revues_link => \@revue_link,
    publications_link => \@publication_link,
    msgs => $msgs,
  };
}

sub vue_file_manager {
  my $id = $_[0];
  my $test_droit_user = $_[1];
  my $msgs = vars->{msgs};

  my $res = schema->resultset('BppPublication')->search({"me.idbpp_publication" => $id});
  my $publication = $res->first();
  # Tester si le dossier existe
  verifExistanceDossier($id);
  my @liste_fichier = glob($cfg->param("site.pathdatapub")."/".$id."/*");
  my %poids_fichier;
  for my $fichier (@liste_fichier) {
    $poids_fichier{$fichier} = (-s $fichier);
  }

  template 'filemanager', {
    publication => $publication,
    test_droit_user => $test_droit_user,
    liste_fichier => \@liste_fichier,
    poids_fichier => \%poids_fichier,
    msgs => $msgs,
  };
}

sub upload_file_pub {
  my $id = $_[0];
  my $uploaded_file = $_[1];


}

sub suppr_file {
  my $file = $_[0];
  unlink $file;
  use File::Basename;
  return "Le fichier ".basename($file)." a été supprimé.";
}
sub verifExistanceDossier {
  my $id = $_[0];

  (-e $cfg->param("site.pathdatapub")) or mkdir($cfg->param("site.pathdatapub"));
  (-e $cfg->param("site.pathdatapub")."/".$id) or mkdir($cfg->param("site.pathdatapub")."/".$id);
}

sub get_auteur_princ {
  my $id_pub = $_[0];
  my $res = schema->resultset('BppManage')->search({'me.idbpp_publication' => $id_pub,
                                                    'me.rank' => "1"});
  my $manage = $res->first();
  my $auteur_princ = $manage->idbpp_user;
  return $auteur_princ;
}
# ------------------------------------------------------------
#  Extraction
# ------------------------------------------------------------

sub extraction_users_go{

  use Spreadsheet::WriteExcel;

  chdir($path) or die "Cant chdir to $path $!";

  # Create a new Excel workbook
  my $workbook  = Spreadsheet::WriteExcel->new('extraction_users.xls');

  extraction_users_function($workbook);

  return 1;
}

sub extraction_users_function{

  my $workbook = $_[0];

  my $worksheet = $workbook->add_worksheet('Utilisateurs');

  my $gras = $workbook->add_format();
  $gras->set_bold();

  my $compteur=1;
  my $row=0;
  my $column=0;

  my $rs = schema->resultset('BppUser')->search({},{order_by => 'me.is_deleted'});
  my @users = $rs->all();

    $worksheet->write($row, $column,  "Login", $gras);
    $column++;
    $worksheet->write($row, $column,  "Prénom", $gras);
    $column++;
    $worksheet->write($row, $column,  "Nom", $gras);
    $column++;
    $worksheet->write($row, $column, "Droits", $gras);
    $column++;
    $worksheet->write($row, $column,  "E-mail", $gras);
    $column++;
    $worksheet->write($row, $column,  "Epi", $gras);
    $column++;
    $worksheet->write($row, $column,  "Position", $gras);
    $column++;
    $worksheet->write($row, $column,  "Supprimé", $gras);

    $row=1;
    $column=0;

  for (@users){
    $worksheet->write($row, $column,  $_->login);
    $column++;
    $worksheet->write($row, $column,  $_->surname);
    $column++;
    $worksheet->write($row, $column,  $_->name);
    $column++;
    $worksheet->write($row, $column,  $_->bpp_usertype_idbpp_usertype->nom);
    $column++;
    $worksheet->write($row, $column,  $_->email);
    $column++;
    $worksheet->write($row, $column,  $_->epi);
    $column++;
    $worksheet->write($row, $column,  $_->position);
    $column++;
    $worksheet->write($row, $column,  $_->is_deleted);
    $column=0;
    $row++;
    $compteur++;
  }

  return 1;
}

sub extraction_journal_go{

  use Spreadsheet::WriteExcel;

  chdir($path) or die "Cant chdir to $path $!";

  # Create a new Excel workbook
  my $workbook  = Spreadsheet::WriteExcel->new('extraction_journaux.xls');

  extraction_journaux_function($workbook);

  return 1;  
}

sub extraction_journaux_function{

  my $workbook = $_[0];

  my $worksheet = $workbook->add_worksheet('Journaux');

  my $gras = $workbook->add_format();
  $gras->set_bold();

  my $compteur=1;
  my $row=0;
  my $column=0;

  my $rs = schema->resultset('BppJournal')->search({},{order_by => 'me.is_deleted'});
  my @journaux = $rs->all();

    $worksheet->write($row, $column,  "Numéro", $gras);
    $column++;
    $worksheet->write($row, $column,  "Titre", $gras);
    $column++;
    $worksheet->write($row, $column,  "Sujet", $gras);
    $column++;
    $worksheet->write($row, $column, "Impact Factor", $gras);
    $column++;
    $worksheet->write($row, $column,  "Lien template", $gras);
    $column++;
    $worksheet->write($row, $column,  "Supprimé", $gras);

    $row=1;
    $column=0;

  for (@journaux){
    $worksheet->write($row, $column,  $_->idbpp_journal);
    $column++;
    $worksheet->write($row, $column,  $_->title);
    $column++;
    $worksheet->write($row, $column,  $_->subject);
    $column++;
    $worksheet->write($row, $column,  $_->impact_factor);
    $column++;
    $worksheet->write($row, $column,  $_->template_link);
    $column++;
    $worksheet->write($row, $column,  $_->is_deleted);
    $column=0;
    $row++;
    $compteur++;
  }

  return 1;
}

sub extraction_correcteur_go{

  use Spreadsheet::WriteExcel;

  chdir($path) or die "Cant chdir to $path $!";

  # Crée un nouveau fichier excel
  my $workbook  = Spreadsheet::WriteExcel->new('extraction_correcteur.xls');

  extraction_correcteur_function($workbook);

  return 1;
}

sub extraction_correcteur_function{

  my $workbook = $_[0];

  # Crée une nouvelle feuille excel
  my $worksheet = $workbook->add_worksheet('Correcteurs');

  my $gras = $workbook->add_format();
  $gras->set_bold();

  my $compteur=1;
  my $row=0;
  my $column=0;

  my $rs = schema->resultset('BppProofreader')->search({},{order_by => 'me.is_deleted'});
  my @correcteurs = $rs->all();

    $worksheet->write($row, $column,  "Numéro", $gras);
    $column++;
    $worksheet->write($row, $column,  "Nom", $gras);
    $column++;
    $worksheet->write($row, $column,  "Coût", $gras);
    $column++;
    $worksheet->write($row, $column, "Temps de correction", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Supprimé", $gras);

    $row=1;
    $column=0;

  for (@correcteurs){
    $worksheet->write($row, $column,  $_->idbpp_proofreader);
    $column++;
    $worksheet->write($row, $column,  $_->pf_name);
    $column++;
    $worksheet->write($row, $column,  $_->pf_cost);
    $column++;
    $worksheet->write($row, $column,  $_->pf_time);
    $column++;

    my $formatter1 = HTML::FormatText->new();
    my $tree1 = HTML::TreeBuilder->new();
    my $tree1 = $tree1->parse_content($_->pf_comment);
    my $commentCorrige = $formatter1->format($tree1);
    $worksheet->write($row, $column,  $commentCorrige);
    $column++;
    $worksheet->write($row, $column,  $_->is_deleted);
    $column=0;
    $row++;
    $compteur++;
  }

  return 1;
}

sub extraction_keyword_go{

  use Spreadsheet::WriteExcel;

  chdir($path) or die "Cant chdir to $path $!";

  # Create a new Excel workbook
  my $workbook  = Spreadsheet::WriteExcel->new('extraction_keyword.xls');

  extraction_keyword_function($workbook);

  return 1;
}

sub extraction_keyword_function{
  my $workbook = $_[0];

  my $worksheet = $workbook->add_worksheet('Mots clés');

  my $gras = $workbook->add_format();
  $gras->set_bold();

  my $compteur=1;
  my $row=0;
  my $column=0;
  my $val=0;
  my $val1=1;
  my $val2=2;


  my $rs = schema->resultset('BppKeyword')->search();
  my @keywords = $rs->all();

    $worksheet->write($row, $column,  "Numéro", $gras);
    $column++;
    $worksheet->write($row, $column,  "Mot clé", $gras);
    $column++;
    $worksheet->write($row, $column,  "Utilisation dans des publications", $gras);
    $column++;
    $worksheet->write($row, $column, "Utilisation dans des revues", $gras);
    $column++;
    $worksheet->write($row, $column,  "Nombre total d'utilisation", $gras);

    $row=1;
    $column=0;

  for (@keywords){

    my $test = $_->idbpp_keyword;
    my $val1 = schema->resultset('BppJourkey')->search({'me.idbpp_keyword' => $test})->count;
    my $val2 = schema->resultset('BppPubkey')->search({'me.idbpp_keyword' => $test})->count;
    $val = $val1+$val2;

    $worksheet->write($row, $column,  $_->idbpp_keyword);
    $column++;
    $worksheet->write($row, $column,  $_->word);
    $column++;
    $worksheet->write($row, $column,  $val1);
    $column++;
    $worksheet->write($row, $column,  $val2);
    $column++;
    $worksheet->write($row, $column,  $val);
    $column=0;
    $row++;
    $compteur++;
  }

  return 1;
}

sub extraction_publication_go{

  use Spreadsheet::WriteExcel;

  chdir($path) or die "Cant chdir to $path $!";

  # Create a new Excel workbook
  my $workbook  = Spreadsheet::WriteExcel->new('extraction_publication.xls');
  
  extraction_publication_function($workbook);

  return 1
}

sub extraction_publication_function{

  my $workbook = $_[0];

  my $worksheet = $workbook->add_worksheet('Publication');

  my $format = $workbook->add_format();
  $format->set_text_wrap();

  my $gras = $workbook->add_format();
  $gras->set_bold();

  my $grasAjuste = $workbook->add_format();
  $grasAjuste->set_bold();
  $grasAjuste->set_text_wrap();


  my $compteur=1;
  my $row=0;
  my $column=0;

  my $rs = schema->resultset('BppPublication')->search({},{order_by => 'me.is_deleted'});
  my @publication = $rs->all();

    $worksheet->write($row, $column,  "Publication supprimée", $grasAjuste);
    $column++;
    $worksheet->write($row, $column,  "Numéro", $gras);
    $column++;
    $worksheet->write($row, $column,  "Titre", $gras);
    $column++;
    $worksheet->write($row, $column,  "Thème", $gras);
    $column++;
    $worksheet->write($row, $column, "Résumé", $gras);
    $column++;
    $worksheet->write($row, $column,  "Idées principales", $grasAjuste);
    $column++;
    $worksheet->write($row, $column,  "Mots clés associés", $grasAjuste);
    $column++;
    $worksheet->write($row, $column,  "Journal", $gras);
    $column++;
    $worksheet->write($row, $column,  "Numéro Journal", $gras);
    $column++;
    $worksheet->write($row, $column,  "Utilisateur référent", $grasAjuste);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Auteur principal", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "2ème auteur", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column, "3ème auteur", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Autres auteurs", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Etat correction", $gras);
    $column++;
    $worksheet->write($row, $column,  "Correcteur", $gras);
    $column++;
    $worksheet->write($row, $column,  "Numéro correcteur", $gras);
    $column++;
    $worksheet->write($row, $column,  "Status (en gras si actuel)", $gras);
    $column++;
    $worksheet->write($row, $column, "Date de début", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin prévu", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin réelle", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Status (en gras si actuel)", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de début", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin prévu", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin réelle", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Status (en gras si actuel)", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de début", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin prévu", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin réelle", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Status (en gras si actuel)", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de début", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin prévu", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin réelle", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Status (en gras si actuel)", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de début", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin prévu", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin réelle", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Status (en gras si actuel)", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de début", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin prévu", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin réelle", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Status (en gras si actuel)", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de début", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin prévu", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin réelle", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Status (en gras si actuel)", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de début", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin prévu", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin réelle", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Status (en gras si actuel)", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de début", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin prévu", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin réelle", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Status (en gras si actuel)", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de début", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin prévu", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin réelle", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Status (en gras si actuel)", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de début", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin prévu", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin réelle", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Status (en gras si actuel)", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de début", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin prévu", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin réelle", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);
    $column++;
    $worksheet->write($row, $column,  "Status (en gras si actuel)", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de début", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin prévu", $gras);
    $column++;
    $worksheet->write($row, $column,  "Date de fin réelle", $gras);
    $column++;
    $worksheet->write($row, $column,  "Commentaire", $gras);

    $row=1;
    $column=0;

  for (@publication){

    my $id_publication = $_->idbpp_publication;

    $worksheet->write($row, $column,  $_->is_deleted);
    $column++;
    $worksheet->write($row, $column,  $id_publication);
    $column++;
    $worksheet->write($row, $column,  $_->title);
    $column++;
    $worksheet->write($row, $column,  $_->subject);
    $column++;

    my $formatter = HTML::FormatText->new();
    my $tree = HTML::TreeBuilder->new();
    my $tree = $tree->parse_content($_->abstract);
    my $abstractCorrige = $formatter->format($tree);
    $worksheet->write($row, $column,  $abstractCorrige);

    $column++;
    $worksheet->write($row, $column,  $_->main_ideas);

    my $resuuuu = schema->resultset('BppPubkey')->search({'me.idbpp_publication' => $id_publication});
    my @keywords = $resuuuu->all();

    # Variable qui contiendra tous les auteurs après le 5ème rang et leur fonction
    my $keywordsn="";

    for (@keywords){
      $keywordsn=$keywordsn.$_->idbpp_keyword->word."\n";
    }
    $column++;
    $worksheet->write($row, $column,  $keywordsn, $format);

    $column=6;

    $column++;
    $worksheet->write($row, $column,  $_->idbpp_journal->title);
    $column++;
    $worksheet->write($row, $column,  $_->idbpp_journal->idbpp_journal);
    
    my $resu = schema->resultset('BppManage')->search({'me.idbpp_publication' => $id_publication},{order_by => 'me.rank'});
    my @publicationUsersManage = $resu->all();

    # Variable qui contiendra tous les auteurs après le 5ème rang et leur fonction
    my $auteurn="";
    my $functionn="";

    for (@publicationUsersManage){
      if ($_->rank<5) {
        $column++;
        my $auteur=$_->idbpp_user->surname." ".$_->idbpp_user->name;
        $worksheet->write($row, $column,  $auteur);
        $column++;
        $worksheet->write($row, $column,  $_->function);
      }
      else {
        $auteurn=$auteurn.$_->idbpp_user->surname." ".$_->idbpp_user->name."\n\n";
        $functionn=$functionn.$_->function."\n\n";
      }
    }
    $column++;
    $worksheet->write($row, $column,  $auteurn, $format);
    $column++;
    $worksheet->write($row, $column,  $functionn, $format);

    $column=18;

    # On récupère les données du correcteur
    my $resuu = schema->resultset('BppProofreading')->search({'me.idbpp_publication' => $id_publication});
    my @publicationCorrecteurs = $resuu->all();

    for (@publicationCorrecteurs){
      $column++;
      $worksheet->write($row, $column,  $_->proofreading_status);
      $column++;
      $worksheet->write($row, $column,  $_->idbpp_proofreader->pf_name);
      $column++;
      $worksheet->write($row, $column,  $_->idbpp_proofreader->idbpp_proofreader);
    }

    my $resuuu = schema->resultset('BppStatus')->search({'me.bpp_publication_idbpp_publication' => $id_publication,'me.status_is_deleted' => "N"});
    my @publicationStatus = $resuuu->all(); 

    my $columnhisto=21;

    for (@publicationStatus){

      $column=$columnhisto;
      my $date_deb = substr $_->beginning_date, 0, 10;
      my $date_estim = substr $_->estimated_date, 0, 10;
      my $date_end = substr $_->final_date, 0, 10;

      my $formatter3 = HTML::FormatText->new();
      my $tree3 = HTML::TreeBuilder->new();
      my $tree3 = $tree3->parse_content($_->status_detail);
      my $commentaire = $formatter3->format($tree3);

      if ($_->status_actuel eq "Y"){
        $column++;
        $worksheet->write($row, $column,  $_->type, $gras);
        $column++;
        $worksheet->write($row, $column,  $date_deb, $gras);
        $column++;
        $worksheet->write($row, $column,  $date_estim, $gras);
        $column++;
        $worksheet->write($row, $column,  $date_end, $gras);
        $column++;
        $worksheet->write($row, $column,  $commentaire, $gras);
      }
      else{
        $column++;
        $worksheet->write($row, $column,  $_->type);
        $column++;
        $worksheet->write($row, $column,  $date_deb);
        $column++;
        $worksheet->write($row, $column,  $date_estim);
        $column++;
        $worksheet->write($row, $column,  $date_end);
        $column++;
        $worksheet->write($row, $column,  $commentaire);
      }

      $columnhisto=$columnhisto+5;
    }

    $column=0;
    $row++;
    $compteur++;
  }

  return 1;
}

sub extraction_totale_go{

  use Spreadsheet::WriteExcel;

  chdir($path) or die "Cant chdir to $path $!";

  # Create a new Excel workbook
  my $workbook  = Spreadsheet::WriteExcel->new('a1_extraction_totale.xls');

  ######################### Users ##########################
  extraction_users_function($workbook);

  ######################### Journaux ##########################
  extraction_journaux_function($workbook);

  ######################### Publication ##########################

  extraction_publication_function($workbook);

  ######################### Correcteurs ##########################

  extraction_correcteur_function($workbook);

  ######################### Mots clés ##########################
  extraction_keyword_function($workbook);

  return 1;
}

# ------------------------------------------------------------
#  THE END
# ------------------------------------------------------------

true;
