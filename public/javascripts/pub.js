/*
BlackPerl is a piece of software designed to help researchers to manage their publications.

Copyright (C) 2014  Maxime CATALDI, Vincent DEMONCHY, Xavier LOCOGE, François de BELLEFON.
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either at version 2 of this licence, or (at your option) any other version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/
*/
CKEDITOR.replace('resume');
$("#keywords").chosen({no_results_text: 'Pas de résultats pour ce mot-clé ! <a class="addkw btn btn-xs btn-info">Ajouter le mot clé</a>'});
$("#autor").chosen({no_results_text: "Pas de résultats !"});
$("#corrector").chosen({no_results_text: "Pas de résultats !"});

$("form").on("click", ".addkw", function() {
  var motcle = $('#keywords_chosen .no-results span').text();

  $('.addkw').text("Ajout en cours");

  $.ajax({
    url: $('body').attr('url-site') + "/keyword/addJS",
    type: 'POST',
    data: {addmotcle: motcle},
  })
  .done(function(msg) {
    if (msg != "-1")
    {
      $('.addkw').text("Mot clé ajouté");
      $('#keywords').append('<option value="'+msg+'">'+motcle+'</option>');
      $("#keywords").trigger("chosen:updated");
      motcle = "";
    }
    else
    {
      $('.addkw').text("Erreur d'ajout");
    }
  })
  .fail(function() {
    $('.addkw').text("Erreur d'ajout");
  })
});

$(".chosen-auteur").each(function (index){
  $(this).chosen({no_results_text: "Pas de résultats !"});
});
$("#revue").chosen({no_results_text: "Pas de résultats !"});

$('.histoCK').each(function (index) {
  CKEDITOR.replace($(this).attr('id'));
});

// Gestion affichage des sous-menus

function affichageSousMenuPubli(){
  $(".to_hide_witj_js").hide();
  $("#myTab").show().after("<hr />");
  $("#autres_auteurs_tab").hide();
  $("#correcteurs_tab").hide();
  $("#revue_tab").hide();
  $("#planning_tab").hide();
  $("div[forid='correcteurs_tab']").hide();
  $("div[forid='autres_auteurs_tab']").hide();
}

affichageSousMenuPubli();
$('#myTab a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');

  var valeurHref = $(this).attr("href");

  $("fieldset").each(function (index){
    if ("#"+$(this).attr("id") == valeurHref){
      $(this).show();
    }
    else{
      $(this).hide();
    }
  });
  $(".form-group[forid]").each(function (index){
    if ("#"+$(this).attr("forid") == valeurHref){
      $(this).show();
    }
    else{
      $(this).hide();
    }
  });
});

$('.form-group[forid] a').click(function (e) {
  e.preventDefault();
  var valeurHref = $(this).attr("href");

  $('#myTab a[href="'+valeurHref+'"]').tab('show');

  $("fieldset").each(function (index){
    if ("#"+$(this).attr("id") == valeurHref){
      $(this).show();
    }
    else{
      $(this).hide();
    }
  });
  $(".form-group[forid]").each(function (index){
    if ("#"+$(this).attr("forid") == valeurHref){
      $(this).show();
    }
    else{
      $(this).hide();
    }
  });
});

$(".histotohide").hide();

// Gestion affichage détails étapes
$('.histohidecontrol').click(function (e) {
  var idHisto = $(this).attr("id-to-hide");

  $(".histotohide[id-to-hide="+idHisto+"]").show();
  $(this).hide();
});

// Gestion ajout d'utilisateurs
$('.addauteur').click(function (e){
  var idAuteurNew= Number($('#hidden_auteur_to_copy').attr('current-id'));

  $(this).before('<hr /><div auteur="'+idAuteurNew+'">'+$('#hidden_auteur_to_copy').html()+'</div>');

  // Gestion des ID/NAME de formulaire
  $('[auteur="'+idAuteurNew+'"] .label_auteur_princ').attr('for', 'autor_sec_'+idAuteurNew);
  $('[auteur="'+idAuteurNew+'"] .label_auteur_info').attr('for', 'autor_sec_'+idAuteurNew+'_info');
  $('[auteur="'+idAuteurNew+'"] .form_auteur_princ').attr({
    name: 'autor_sec_'+idAuteurNew,
    id: 'autor_sec_'+idAuteurNew
  });
  $('[auteur="'+idAuteurNew+'"] .form_auteur_info').attr({
    name: 'autor_sec_'+idAuteurNew+'_info',
    id: 'autor_sec_'+idAuteurNew+'_info'
  });
  // Relancement de Chosen
  $('[auteur="'+idAuteurNew+'"] .form_auteur_princ').chosen({no_results_text: "Pas de résultats !"});

  // Maj de l'ID suivant
  $('#hidden_auteur_to_copy').attr('current-id',idAuteurNew+1);
});

// Récupération de la date du jour en javascript
function get_date_actuelle()
{
        var date = new Date();
        var annee = date.getFullYear();
        var mois = date.getMonth();
        var jour = date.getDate();
        mois=mois+1;
        if(jour<10)
        {
                jour = "0"+jour;
        }
        if(mois<10)
        {
                mois = "0"+mois;
        }
        var resultat = annee+'-'+mois+'-'+jour;
        return resultat;
}

// Gestion Ajout d'étapes

$('.addhisto').click(function (e){
  var idHistoNew = Number($(this).attr('current-id'))+1;

  $(this).attr('current-id', idHistoNew);

  var htmlContentNewHisto = '<table  class="table table-hover table-striped">';
  htmlContentNewHisto +=  '<thead>';
  htmlContentNewHisto +=    '<tr>';
  htmlContentNewHisto +=      '<th colspan="3">';
  htmlContentNewHisto +=        '<select name="histoname_'+idHistoNew+'" id="histoname_'+idHistoNew+'" class="form-control">';
  htmlContentNewHisto +=          '<option value="" selected="selected">(Vide)</option>';
  htmlContentNewHisto +=          '<option value="Rédaction" >Rédaction</option>';
  htmlContentNewHisto +=          '<option value="Soumis" >Soumis</option>';
  htmlContentNewHisto +=          '<option value="Modification" >Modification</option>';
  htmlContentNewHisto +=          '<option value="Re-soumis" >Re-soumis</option>';
  htmlContentNewHisto +=          '<option value="Accepté" >Accepté</option>';
  htmlContentNewHisto +=          '<option value="Publié" >Publié</option>';
  htmlContentNewHisto +=          '<option value="Rejeté" >Rejeté</option>';
  htmlContentNewHisto +=        '</select>';
  htmlContentNewHisto +=      '</th>';
  htmlContentNewHisto +=    '</tr>';
  htmlContentNewHisto +=    '<tr>';
  htmlContentNewHisto +=      '<th>';
  htmlContentNewHisto +=        '<label for="selectedstatut">Etat actuel de la publication</label>';
  htmlContentNewHisto +=      '</th>';
  htmlContentNewHisto +=      '<td colspan="2">';
  htmlContentNewHisto +=        '<input type="checkbox" name="selectedstatut_'+idHistoNew+'" id="selectedstatut_'+idHistoNew+'" value="'+idHistoNew+'" checked/>';
  htmlContentNewHisto +=      '</td>';
  htmlContentNewHisto +=     '</tr>';
  htmlContentNewHisto +=  '</thead>';
  htmlContentNewHisto +=  '<tbody>';
  htmlContentNewHisto +=    '<tr>';
  htmlContentNewHisto +=      '<th>Date de début théorique</th>';
  htmlContentNewHisto +=      '<th>Date de fin prévue</th>';
  htmlContentNewHisto +=      '<th>Date de fin réelle</th>';
  htmlContentNewHisto +=    '</tr>';
  htmlContentNewHisto +=    '<tr>';
  htmlContentNewHisto +=      '<td><input class="datepicker form-control" type="date" name="datedebpre_'+idHistoNew+'" id="datedebpre_'+idHistoNew+'" value="'+get_date_actuelle()+'"></td>';
  htmlContentNewHisto +=      '<td><input class="datepicker form-control" type="date" name="datefinpre_'+idHistoNew+'" id="datefinpre_'+idHistoNew+'" value="'+get_date_actuelle()+'"></td>';
  htmlContentNewHisto +=      '<td><input class="datepicker form-control" type="date" name="datefinrel_'+idHistoNew+'" id="datefinrel_'+idHistoNew+'"></td>';
  htmlContentNewHisto +=    '</tr>';
  htmlContentNewHisto +=    '<tr>';
  htmlContentNewHisto +=      '<th colspan="3">';
  htmlContentNewHisto +=        'Commentaires :';
  htmlContentNewHisto +=      '</th>';
  htmlContentNewHisto +=    '</tr>';
  htmlContentNewHisto +=    '<tr class="histotohide" id-to-hide="'+idHistoNew+'">';
  htmlContentNewHisto +=      '<td colspan="3"><textarea id="histo'+idHistoNew+'" name="histo'+idHistoNew+'" class="control-form"></textarea></td>';
  htmlContentNewHisto +=    '</tr>';
  htmlContentNewHisto +=  '</tbody>';
  htmlContentNewHisto +='</table>';

  $(this).before(htmlContentNewHisto);
  CKEDITOR.replace('histo'+idHistoNew);
});


//$(".datepicker").datepicker();