# Projet Black Perl

Ce projet est réalisé dans le cadre de notre formation aux Mines d'Albi.
Il s'agit d'un gestionnaire de publications à destination de laboratoires de recherches

Version : Pre-Release 2 (Build 314)

__Démonstration :__ [Démo de la pré-release 1](http://bpp.demonchy.eu/)

# Auteurs :
* Cataldi Maxime
* De Meric De Bellefon François
* Demonchy Vincent
* Locoge Xavier

# Installation :
## Serveur
Cette application a été conçue afin d'être déployée en tant qu'application CGI d'un serveur Apache2.

## Modules Perl nécessaires
* Dancer
* URI
* URI::QueryParam
* Dancer::Plugin::DBIC
* DBI
* DateTime
* DateTime::Format::HTTP
* Time::localtime
* Digest::SHA1
* Config::Simple
* Data::Dumper
* DBIx::Class::QueryLog
* DBIx::Class::QueryLog::Analyzer
* CGI::Carp
* File::Slurp
* HTML::FormatText
* HTML::TreeBuilder
* MIME::Lite
* MIME::Lite::TT::HTML
* Spreadsheet::WriteExcel

## Base de données
Le système de gestion de base de données utilisé est MySQL (Engine : InnoDB), il est donc pré-requis de posséder 
une base de données pré-installée vide avec ses utilisateurs.

L'ensemble de la base de données peut être générée à partir du fichier generate.sql dans le dossier
`SQL/`. Afin de pouvoir se connecter à l'application un utilisateur est directement généré :
```
user : admin
mot de passe : admin
```
(Pensez à supprimer cet utilisateur une fois que de nouveaux administrateurs saisis dans l'application)

Par la suite, à l'aide du fichier `SQL/schema-dumper.pl`, les objets permettant la communication avec la 
base de données peuvent être générés et placés dans `lib/BD/`. Cependant, il faudra paramétrer les champs suivants :
```
:::perl
	my $dsn = 'dbi:mysql:blackperl:xxx.xxxxxxxx.xxx';
	my $user = 'xxx_user';
	my $password = 'xxx_password';
```

## Cron
### emailalert.pl
Ce fichier CRON (contenu dans `cron/emailalert.pl`) permet d'alerter les utilisateurs et les adminsitrateurs lorsque les tâches en cours sont 
bientôt à leur date de fin prévue. Une alerte est lancée 14, 7, et 0 jours avant la date de fin prévisionnelle.

Afin de permettre un fonctionnement correct de ce cron, il est conseillé de la paramétrer pour une exécution journalière à une heure creuse (mais opérationnelle) du système.

Exemple de configuration cron pour un lancement journalier à 3h00 :

`0 3 * * *  /usr/lib/cgi-bin/bp/BlackPerl/cron/emailalert.pl`

## Paramétrage
Finalement, il ne reste à modifier que les fichiers de configuration de notre application. 
### config.ini
```
[site]
pathexcel="/usr/lib/cgi-bin/bp/BlackPerl/data/exportsExcel"
pathdatapub="/usr/lib/cgi-bin/bp/BlackPerl/data/pub"
pathtemplatemail="/usr/lib/cgi-bin/bp/BlackPerl/views/mail"
uri_with_dispatch="http://bp.demonchy.eu/dispatch.cgi"

[mysql]
dsn="DBI:mysql:database=blackperl:host=kim.demonchy.eu"
user="bpp_distant"
password="pl1ok2ij3"

[email]
from="vinksyunit@gmail.com"
```

Ce fichier de configuration est un fichier de configuration standard pour notre application, voici les différentes lignes :

* __pathexcel__ (`pathexcel="/usr/lib/cgi-bin/bp/BlackPerl/data/exportsExcel"`): Emplacement du dossier de stockage *temporaire* des fichiers d'exportation au format Excel.

* __pathdatapub__ (`pathdatapub="/usr/lib/cgi-bin/bp/BlackPerl/data/pub"`): Emplacement du dossier de stockage des fichiers attachés aux publications.

* __pathtemplatemail__ (`pathtemplatemail="/usr/lib/cgi-bin/bp/BlackPerl/views/mail"`): Emplacement des templates utilisées pour l'envoi de mails.

* __uri_with_dispatch__ (`uri_with_dispatch="http://bp.demonchy.eu/dispatch.cgi"`): Adresse URL du fichier dispatch.cgi (principalement utilisée par la génération de mail dans `emailalert.pl`)

* __dsn__ (`dsn="DBI:mysql:database=blackperl:host=xxx.xxxxxxx.xx"`): DSN vers la base de données.

* __user__ (`uri_with_dispatch="http://bp.demonchy.eu/dispatch.cgi"`): Adresse URL du fichier dispatch.cgi (principalement utilisée par la génération de mail dans `emailalert.pl`)

* __password__ (`user="xxxxx"`): Mot de passe de la base de données.

* __from__ (`from="xxxxxx@gmail.com"`): Email de l'expéditeur des mails envoyés par le système.

### config.yml
```
appname: "BlackPerl"

layout: "connected"

charset: "UTF-8"
session: "YAML"

template: "template_toolkit"
engines:
  template_toolkit:
    start_tag: '[%'
    end_tag:   '%]'

plugins:
    DBIC:
        DbGestCli:
            schema_class: DB::BPDatabase
            dsn: dbi:mysql:blackperl
            user: bpp_distant
            pass: pl1ok2ij3
            options:
                RaiseError: 1
                PrintError: 1
                mysql_enable_utf8: 1

```

Le fichier de configuration `config.yml` est le fichier de configuration de Dancer. Il permet de renseigner 
les différentes informations requises  par Dancer.  Afin de permettre le fonctionnement correcte de l'application, seul 
les paramètres suivants sont à renseigner :

* __plugins > DBIC > DbGestCli > schema_class__ : Le nom de la classe utilisée pour le schema de base de données. Ici par défaut : 
`schema_class: DB::BPDatabase`.

* __plugins > DBIC > DbGestCli > dsn__ : Le DSN de la basse de données. Ex : `dsn: dbi:mysql:blackperl`.

* __plugins > DBIC > DbGestCli > user__ : L'utilisateur de la base de données.

* __plugins > DBIC > DbGestCli > pass__ : Le mot de passe de la base de données.

# Documentation technique
## Routes Blackperl.pm
### GET '/'

Accès : *Tous*

Page d'accès par défaut, elle redirige vers la page de connexion en cas d'utilisateur non connecté, sinon elle redirige vers la page de visualisation de ses publications.

### GET/POST '/pub/show'

Accès : *Utilisateur*

Page de visualisation des publications concernant l'utilisateur connecté.

### GET '/pub/show/admin'

Accès : *Administrateur*

Page de visualisation des publications concerant l'ensemble des utilisateurs. Possibilité de récupérer depuis cette page les publications supprimées.

### GET '/pub/show/(id)'

Accès : *Utilisateur*

Page de visualisation de la publication portant l'*id* renseigné (lecture seule).

### GET '/pub/add'

Accès : *Utilisateur*

Formulaire d'ajout d'une publication.

### POST '/pub/add'

Accès : *Utilisateur*

Route d'ajout d'une publication.

### GET '/pub/edit/(id)'

Accès : *Référent et Admin*

Formulaire d'edition de la publication avec l'*id* renseignée.

### POST '/pub/edit/(id)'

Accès : *Référent et Admin*

Route d'edition de la publication avec l'*id* renseignée

### GET '/pub/duplicate/(id)'

Accès : *Référent et Admin*

Formulaire de duplication d'une publication avec l'identifiant *id*

### POST '/pub/duplicate/(id)'

Accès : *Référent et Admin*

Route de confirmation de duplication de la publication *id*

### GET '/pub/remove/(id)'

Accès : *Référent et Admin*

Page de demande de confirmation de suppression de la publication *id*

### POST '/pub/remove/(id)'

Accès : *Référent et Admin*

Page de suppresssion de la publication *id*

### GET '/pub/recover/(id)'

Accès : *Admin*

Page de demande de confirmation de récupération de la publication *id* supprimée.

### POST '/pub/recover/(id)'

Accès : *Admin*

Page de récupération de la publication *id* supprimée.

### GET '/file/(id)'

Accès : *Utilisateur*

Page de gestionnaire de fichiers de la publication *id*

### POST '/file/(id)/upload'

Accès : *Admin et référent*

Route d'upload de fichier

### GET '/file/(id)/downloadpdf/(name)'

Accès : *Utilisateur*

Route qui renvoi le contenu d'un PDF en sortie afin de pouvoir l'afficher directement dans le lecteur.

### GET '/file/(id)/download/(name)'

Accès : *Utilisateur*

Route qui renvoi le fichier à l'utilisateur

### GET '/co'

Accès : *Tous*

Envoi le formulaire de connexion

### POST '/co'

Accès : *Tous*

Envoi les information de connexion et redirige l'utilisateur en fonction de 

### GET '/co/pass'

Accès : *Tous*

Permet d'accéder à la page de demande de ré-initialisation de mot de passe.

### POST '/co/pass'

Accès : *Tous*

Traite la demande de ré-initialisation du mot de passe en envoyant un email 
avec un mot de passe aléatoire valide pour la journée.

### GET '/user/list'

Accès : *Admin*

Permet d'accéder à la liste des utilisateurs de l'application.

### POST '/user/add'

Accès : *Admin*

Permet d'ajout un utilisateur.

### GET '/user/add'

Accès : *Admin*

Permet d'accéder au formulaire d'ajout d'un utilisateur.

### GET '/user/edit/(id)'

Accès : *Admin*

Permet d'accéder au formulaire d'édition d'un utilisateur *id*.

### POST '/user/edit/(id)'

Accès : *Admin*

Permet d'éditer l'utilisateur *id*

### GET '/profil'

Accès : *Utilisateur*

Permet d'accéder au formulaire d'édition de son profil utilisateur.

### POST '/profil'

Accès : *Admin*

Permet d'éditer son profil utilisateur.

### GET '/user/recover/(id)'

Accès : *Admin*

Permet de récupérer un utilisateur *id* précédemment supprimé.

### POST '/user/suppr/(id)'

Accès : *Admin*

Permet de supprimer un utilisateur *id*

### GET/POST '/keyword'

Accès : *Utilisateur*

Permet d'accéder à la liste des mots-clé.

### POST '/keyword/add'

Accès : *Utilisateur*

Permet d'ajouter un mot-clé

### POST '/keyword/addJS'

Accès : *Utilisateur*

Route permettant d'ajouter dynamiquement des mots-clé avec du Javascript.

### GET '/keyword/(id)'

Accès : *Utilisateur*

Permet d'accéder à la liste des utilisation du keyword *id*

### GET/POST '/corr'

Accès : *Utilisateur*

Permet d'accéder à la liste des correcteurs

### GET '/corr/edit/(id)'

Accès : *Admin*

Permet d'accéder au formulaire d'édition du correcteur *id*

### GET '/corr/recover/(id)'

Accès : *Admin*

Permet de récupérer le correcteur *id*

### POST '/corr/edit/(id)'

Accès : *Admin*

Permet d'éditer le correcteur *id*

### GET '/corr/add'

Accès : *Admin*

Permet d'accéder au formulaire d'ajout d'un correcteur

### POST '/corr/add'

Accès : *Admin*

Permet d'ajouter un correcteur

### GET '/corr/suppr/(id)'

Accès : *Admin*

Affichage de la demande de confirmation de suppression d'un correcteur

### POST '/corr/suppr/(id)'

Accès : *Admin*

Suppression d'un correcteur

### GET '/revues'

Accès : *Utilisateur*

Permet d'accéder à la liste des revues

### GET '/revues/view/(id)'

Accès : *Utilisateur*

Permet d'afficher la revue *id*

### GET '/revues/add'

Accès : *Admin*

Permet d'accéder au formulaire pour ajouter une revue

### POST '/revues/add'

Accès : *Admin*

Permet d'ajouter une revue

### GET '/revues/edit/(id)'

Accès : *Admin*

Permet d'accéder au formulaire pour éditer une revue

### POST '/revues/edit/(id)'

Accès : *Admin*

Permet d'éditer une revue

### GET '/revues/suppr/(id)'

Accès : *Admin*

Permet d'accéder à la page de suppression d'une revue

### POST '/revues/suppr/(id)'

Accès : *Admin*

Permet de supprimer une revue

### GET '/revues/recover/(id)'

Accès : *Admin*

Permet d'accéder à la page de restauration d'une revue *id*

### POST '/revues/recover/(id)'

Accès : *Admin*

Permet de restaurer la revue *id*

### GET/POST '/extract'

Accès : *Admin*

Permet d'accéder à la liste des extractions.

### GET '/extract/users'

Accès : *Admin*

Permet d'extraire au format Excel les utilisateur.

### GET '/extract/journaux'

Accès : *Admin*

Permet d'extraire au format Excel les revues.

### GET '/extract/correcteurs'

Accès : *Admin*

Permet d'extraire au format Excel les correcteurs.

### GET '/extract/keywords'

Accès : *Admin*

Permet d'extraire au format Excel les mots clés.

### GET '/extract/publications'

Accès : *Admin*

Permet d'extraire au format Excel les publications.

### GET '/extract/all'

Accès : *Admin*

Permet d'extraire au format Excel l'ensemble des données.

### GET/POST '.*'

Page d'erreur 404

## Plugins

### Bootstrap

Ce plugin situé à l'emplacement `public/plugins/bootstrap/` est un ensemble composé d'une feuille de style CSS et d'un script JQuery permettant d'harmoniser l'affichage en fonction de l'appareil utilisé.

### Chosen

Ce plugin situé à l'emplacement `public/plugins/chosen/` permet de dynamiser les saisies de formulaire à l'aide de recherches dynamiques et autres surcouches de sélection. Il est notamment utilisé dans le cadre de la sélection de mot-clés.

### CKEditor

Ce plugin situé à l'emplacement `public/plugins/chosen/` permet de saisir du texte à l'aide de champs de saisie riches, proches d'un éditeur de documents.

### JQuery

Très célèbre librairie Javascript, ici dans sa version 2. Installé dans `public/javascripts/jquery.js`.

### JQuery TagCloud

Permet de générer des nuages de tags. Installé dans `public/javascripts/jquery.tagcloud.js`.

### JQuery StupidTable

Permet de trier les tables dynamiquement. Installé dans `public/javascripts/stupidtable.min.js`.

# Licence

BlackPerl is a piece of software designed to help researchers to manage their publications.


Copyright (C) 2014  Maxime CATALDI, Vincent DEMONCHY, Xavier LOCOGE, François de BELLEFON.
This program is free software: you can redistribute it and/or modify
it under the terms of the MIT License or the GNU General Public License as published by
the Free Software Foundation, either at version 2 of this licence, or (at your option) any other version.


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.


You should have received a copy of the GNU General Public License
along with this program.  If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/)